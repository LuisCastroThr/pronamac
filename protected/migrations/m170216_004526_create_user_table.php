<?php

class m170216_004526_create_user_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('pnc_users', array(
            'id' 		=> 'pk',
			'name' 		=> 'varchar(100) NOT NULL',
			'lastname' 	=> 'varchar(100) DEFAULT NULL',
			'email' 	=> 'varchar(100) NOT NULL',
			'password'  => 'varchar(200) DEFAULT NULL',
			'create' 	=> 'timestamp',
			'last_login'=> 'timestamp',
			'type_user' => 'int(2) DEFAULT 0'
        ));
	}

	public function down()
	{
		echo "m170216_004526_create_user_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}