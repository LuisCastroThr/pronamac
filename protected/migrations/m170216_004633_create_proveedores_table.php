<?php

class m170216_004633_create_proveedores_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('pnc_proveedores', array(
            'id' 			=> 'pk',
			'name' 			=> 'varchar(100) NOT NULL',
			'descripcion' 	=> 'varchar(500) DEFAULT NULL',
			'direccion' 	=> 'varchar(300) DEFAULT NULL',
			'telefono' 		=> 'varchar(50) NOT NULL',
			'rfc' 			=> 'varchar(50) DEFAULT NULL',
			'create' 		=> 'timestamp',
        ));
	}

	public function down()
	{
		echo "m170216_004633_create_proveedores_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}