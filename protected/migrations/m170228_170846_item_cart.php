<?php

class m170228_170846_item_cart extends CDbMigration
{
	public function up()
	{
		$this->createTable('pnc_item_cart', array(
            'id' 				=> 'pk',
			'id_carrito' 	    => 'int(10) NOT NULL',
			'id_producto' 	    => 'int(10) NOT NULL',
			'cantidad' 			=> 'int(10) NOT NULL',
			'monto' 		 	=> 'bigint(20) NOT NULL',
			'create' 			=> 'timestamp',
			'update' 			=> 'timestamp'
        ));
	}

	public function down()
	{
		echo "m170228_170846_item_cart does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}