<?php

class m170918_061015_facturas extends CDbMigration
{
	public function up()
	{
		$this->createTable('pnc_facturas', array(
            'id' 				=> 'pk',
			'id_usuario' 	    => 'int(10) NOT NULL',
			'razon' 		    => 'text DEFAULT NULL',
			'calle' 		    => 'text DEFAULT NULL',
			'ext_numero' 		=> 'text DEFAULT NULL',
			'int_numero' 		=> 'text DEFAULT NULL',
			'rfc'   		 	=> 'varchar(10) NOT NULL',
			'tipo'   		 	=> 'varchar(10) NOT NULL',
			'codigo_postal' 	=> 'varchar(10) NOT NULL',
			'estado' 			=> 'varchar(100) NOT NULL',
			'ciudad' 			=> 'varchar(100) NOT NULL',
			'delegacion' 		=> 'varchar(100) NOT NULL',
			'colonia' 			=> 'varchar(100) NOT NULL',
			'correo' 			=> 'varchar(100) NOT NULL',
			'telefono' 			=> 'text DEFAULT NULL'
        ));
	}

	public function down()
	{
		echo "m170918_061015_facturas does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}