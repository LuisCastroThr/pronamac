<?php

class m170224_015935_productos extends CDbMigration
{
	public function up()
	{
		$this->createTable('pnc_productos', array(
            'id' 				=> 'pk',
			'codigo' 			=> 'varchar(10) NOT NULL',
			'nombre' 			=> 'varchar(100) NOT NULL',
			'descripcion_gral' 	=> 'text DEFAULT NULL',
			'descripcion_pres' 	=> 'text DEFAULT NULL',
			'presentacion' 		=> 'varchar(500) DEFAULT NULL',
			'piezas_x_unidad' 	=> 'varchar(500) NOT NULL',
			'unidad' 	 		=> 'int(10) NOT NULL',
			'id_categoria' 	 	=> 'int(10) NOT NULL',
			'id_proveedor' 	 	=> 'int(10) NOT NULL',
			'precio' 		 	=> 'float(20) NOT NULL',
			'image' 			=> 'varchar(100) DEFAULT NULL',
			'pdf' 				=> 'varchar(100) DEFAULT NULL',
			'create' 			=> 'timestamp',
        ));
	}

	public function down()
	{
		echo "m170224_015935_productos does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}