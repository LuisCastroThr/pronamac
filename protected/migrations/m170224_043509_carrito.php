<?php

class m170224_043509_carrito extends CDbMigration
{
	public function up()
	{
		$this->createTable('pnc_carrito', array(
            'id' 				=> 'pk',
			'id_user' 	        => 'int(10) DEFAULT NULL',
			'fingerprint' 	    => 'varchar(150) DEFAULT NULL',
			'total' 		 	=> 'float(20) NOT NULL',
			'status' 			=> 'TINYINT NOT NULL',
			'create' 			=> 'timestamp',
			'update' 			=> 'timestamp',
			'type_send' 		=> 'int(10) DEFAULT NULL',
			'invoice' 			=> 'int(10) DEFAULT NULL'
        ));
	}

	public function down()
	{
		echo "m170224_043509_carrito does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/

	//ALTER TABLE pnc_carrito ADD type_send int(10) DEFAULT NULL;
	//ALTER TABLE pnc_carrito ADD invoice int(10) DEFAULT NULL;
	//ALTER TABLE pnc_carrito MODIFY COLUMN total float(20) NOT NULL;
}