<?php

class m170222_022147_categorias extends CDbMigration
{
	public function up()
	{
		$this->createTable('pnc_categorias', array(
            'id' 		=> 'pk',
			'name' 		=> 'varchar(100) NOT NULL',
			'description' 	=> 'varchar(500) DEFAULT NULL',
        ));
	}

	public function down()
	{
		echo "m170222_022147_categorias does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}