<?php 
class ProcesosCommand extends CConsoleCommand
{
	public function actionCargaProductos()
	{
		if (($fichero = fopen(Yii::app()->params['rutaData']."datos_1.csv", "r")) !== FALSE)
        {
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE)
            {
        		if(!empty($datos))
        		{
	        		$producto = new PncProductos();
	        		if(isset($datos[1]))
	        			$producto->codigo 			= $datos[1];

					if(isset($datos[2]))
						$producto->nombre 			= $datos[2];

					if(isset($datos[3]))
						$producto->descripcion_gral = $datos[3];

					if(isset($datos[4]))
						$producto->descripcion_pres = $datos[4];

					if(isset($datos[5]))
						$producto->presentacion     = $datos[5];

					if(isset($datos[6]))
						$producto->piezas_x_unidad  = $datos[6];

					if(isset($datos[7]))
						$producto->unidad 			= $datos[7];

					if(isset($datos[8]))
						$producto->id_categoria 	= Tools::getCategorias($datos[8]);

					if(isset($datos[9]))
						$producto->id_proveedor 	= $datos[9];

					if(isset($datos[10]))
						$producto->precio 			= $datos[10];

					if(isset($datos[11]))
						$producto->image 			= $datos[11];

					if(isset($datos[12]))
						$producto->pdf 				= $datos[12];

					$producto->create 			= date('Y-m-d');
					// if( !$producto->save() )
					// {
					// 	print_r($datos);
					// 	print_r($producto->getErrors());
					// }
				}
				else
				{
					print_r($datos);
					echo 'Vacio';
				}
            }
        }
	}
}
?>