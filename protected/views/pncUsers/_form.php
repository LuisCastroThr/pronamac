<?php
/* @var $this PncUsersController */
/* @var $model PncUsers */
/* @var $form CActiveForm */
?>

<div class="">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'pnc-users-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
	)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="small-6 large-6 columns">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>

		<div class="small-6 large-6 columns">
			<?php echo $form->labelEx($model,'lastname'); ?>
			<?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'lastname'); ?>
		</div>
	</div>

	<div class="row">
		<div class="small-6 large-6 columns">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>

		<div class="small-6 large-6 columns">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>200)); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
	</div>

	<div class="row">
		<div class="small-6 large-6 columns">
			<?php echo $form->labelEx($model,'create'); ?>
			<?php echo $form->textField($model,'create'); ?>
			<?php echo $form->error($model,'create'); ?>
		</div>

		<div class="small-6 large-6 columns">
			<?php echo $form->labelEx($model,'last_login'); ?>
			<?php echo $form->textField($model,'last_login'); ?>
			<?php echo $form->error($model,'last_login'); ?>
		</div>
	</div>

	<div class="row">
		<div class="small-6 large-6 columns">
			<?php echo $form->labelEx($model,'type_user'); ?>
			<?php echo $form->textField($model,'type_user'); ?>
			<?php echo $form->error($model,'type_user'); ?>
		</div>
	</div>

	<div class="">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Guardar cambios',  array('class' => 'button expanded')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>
<!-- form -->
