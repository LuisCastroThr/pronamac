<?php
/* @var $this PncUsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pnc Users',
);

$this->menu=array(
	array('label'=>'Crear Usuario', 'url'=>array('create')),
	array('label'=>'Administrar Usuario', 'url'=>array('admin')),
);
?>

<h1 class="h1-text">Lista de Usuarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
