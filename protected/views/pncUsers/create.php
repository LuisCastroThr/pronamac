<?php
/* @var $this PncUsersController */
/* @var $model PncUsers */

$this->breadcrumbs=array(
	'Pnc Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista de Usuarios', 'url'=>array('index')),
	array('label'=>'Administrar Usuario', 'url'=>array('admin')),
);
?>

<h1 class="h1-text">Crear Usuarios</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
