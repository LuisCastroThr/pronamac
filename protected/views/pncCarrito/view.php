<?php
/* @var $this PncCarritoController */
/* @var $model PncCarrito */

$this->breadcrumbs=array(
	'Pnc Carritos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PncCarrito', 'url'=>array('index')),
	array('label'=>'Create PncCarrito', 'url'=>array('create')),
	array('label'=>'Update PncCarrito', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PncCarrito', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PncCarrito', 'url'=>array('admin')),
);
?>

<h1>View PncCarrito #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_producto',
		'id_user',
		'fingerprint',
		'cantidad',
		'total',
		'status',
		'create',
		'update',
	),
)); ?>
