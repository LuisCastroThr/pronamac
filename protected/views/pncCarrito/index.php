<?php
/* @var $this PncCarritoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pnc Carritos',
);

$this->menu=array(
	array('label'=>'Create PncCarrito', 'url'=>array('create')),
	array('label'=>'Manage PncCarrito', 'url'=>array('admin')),
);
?>

<h1>Pnc Carritos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
