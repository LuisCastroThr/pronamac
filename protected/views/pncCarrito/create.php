<?php
/* @var $this PncCarritoController */
/* @var $model PncCarrito */

$this->breadcrumbs=array(
	'Pnc Carritos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PncCarrito', 'url'=>array('index')),
	array('label'=>'Manage PncCarrito', 'url'=>array('admin')),
);
?>

<h1>Create PncCarrito</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>