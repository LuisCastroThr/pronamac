<?php
/* @var $this PncCarritoController */
/* @var $model PncCarrito */

$this->breadcrumbs=array(
	'Pnc Carritos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PncCarrito', 'url'=>array('index')),
	array('label'=>'Create PncCarrito', 'url'=>array('create')),
	array('label'=>'View PncCarrito', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PncCarrito', 'url'=>array('admin')),
);
?>

<h1>Update PncCarrito <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>