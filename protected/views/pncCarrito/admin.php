<?php
/* @var $this PncCarritoController */
/* @var $model PncCarrito */

$this->breadcrumbs=array(
	'Pnc Carritos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PncCarrito', 'url'=>array('index')),
	array('label'=>'Create PncCarrito', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pnc-carrito-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pnc Carritos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pnc-carrito-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'id_producto',
		'id_user',
		'fingerprint',
		'cantidad',
		'total',
		/*
		'status',
		'create',
		'update',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
