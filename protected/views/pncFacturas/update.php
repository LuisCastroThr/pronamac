<?php
/* @var $this PncFacturasController */
/* @var $model PncFacturas */

$this->breadcrumbs=array(
	'Pnc Facturases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PncFacturas', 'url'=>array('index')),
	array('label'=>'Create PncFacturas', 'url'=>array('create')),
	array('label'=>'View PncFacturas', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PncFacturas', 'url'=>array('admin')),
);
?>

<h1>Update PncFacturas <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>