<?php
/* @var $this PncFacturasController */
/* @var $model PncFacturas */

$this->breadcrumbs=array(
	'Pnc Facturases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PncFacturas', 'url'=>array('index')),
	array('label'=>'Manage PncFacturas', 'url'=>array('admin')),
);
?>

<h1>Create PncFacturas</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>