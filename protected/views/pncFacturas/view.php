<?php
/* @var $this PncFacturasController */
/* @var $model PncFacturas */

$this->breadcrumbs=array(
	'Pnc Facturases'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PncFacturas', 'url'=>array('index')),
	array('label'=>'Create PncFacturas', 'url'=>array('create')),
	array('label'=>'Update PncFacturas', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PncFacturas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PncFacturas', 'url'=>array('admin')),
);
?>

<h1>View PncFacturas #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_usuario',
		'razon',
		'calle',
		'ext_numero',
		'int_numero',
		'rfc',
		'tipo',
		'codigo_postal',
		'estado',
		'ciudad',
		'delegacion',
		'colonia',
		'correo',
		'telefono',
	),
)); ?>
