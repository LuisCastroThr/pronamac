<?php
/* @var $this PncProductosController */
/* @var $model PncProductos */

$this->breadcrumbs=array(
	'Pnc Productoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Lista de Productos', 'url'=>array('index')),
	array('label'=>'Crear Productos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pnc-productos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1 class="h1-text">Administrar Productos</h1>

<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p> -->

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pnc-productos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'codigo',
		'nombre',
		'descripcion_gral',
		'descripcion_pres',
		'presentacion',
		/*
		'piezas_x_unidad',
		'unidad',
		'id_categoria',
		'id_proveedor',
		'precio',
		'image',
		'pdf',
		'create',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
