<?php
/* @var $this PncProductosController */
/* @var $model PncProductos */

$this->breadcrumbs=array(
	'Pnc Productoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Lista de Productos', 'url'=>array('index')),
	array('label'=>'Crear Productos', 'url'=>array('create')),
	array('label'=>'Actualizar Productos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Productos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Productos', 'url'=>array('admin')),
);
?>

<h1 class="h1-text">Productos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'codigo',
		'nombre',
		'descripcion_gral',
		'descripcion_pres',
		'presentacion',
		'piezas_x_unidad',
		'unidad',
		'id_categoria',
		'id_proveedor',
		'precio',
		'image',
		'pdf',
		'create',
	),
)); ?>
