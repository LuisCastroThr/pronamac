<?php
/* @var $this PncProductosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pnc Productoses',
);

$this->menu=array(
	array('label'=>'Crear Productos', 'url'=>array('create')),
	array('label'=>'Administrar Productos', 'url'=>array('admin')),
);
?>

<h1 class="h1-text">Productos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
