<?php
/* @var $this PncProductosController */
/* @var $model PncProductos */

$this->breadcrumbs=array(
	'Pnc Productoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista de Productos', 'url'=>array('index')),
	array('label'=>'Administrar Productos', 'url'=>array('admin')),
);
?>

<h1 class="h1-text">Crear Productos</h1>

<?php
	$this->renderPartial('_form', array(
		'model' 		=> $model,
		'proveedores' 	=> $proveedores,
		'categorias' 	=> $categorias
	));
?>
