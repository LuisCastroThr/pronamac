<?php
/* @var $this PncProductosController */
/* @var $model PncProductos */

$this->breadcrumbs=array(
	'Pnc Productoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista de Productos', 'url'=>array('index')),
	array('label'=>'Crear Productos', 'url'=>array('create')),
	array('label'=>'Ver Productos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Productos', 'url'=>array('admin')),
);
?>

<h1 class="h1-text">Actualizar Productos <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
