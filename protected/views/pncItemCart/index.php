<?php
/* @var $this PncItemCartController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pnc Item Carts',
);

$this->menu=array(
	array('label'=>'Create PncItemCart', 'url'=>array('create')),
	array('label'=>'Manage PncItemCart', 'url'=>array('admin')),
);
?>

<h1>Pnc Item Carts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
