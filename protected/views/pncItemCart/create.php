<?php
/* @var $this PncItemCartController */
/* @var $model PncItemCart */

$this->breadcrumbs=array(
	'Pnc Item Carts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PncItemCart', 'url'=>array('index')),
	array('label'=>'Manage PncItemCart', 'url'=>array('admin')),
);
?>

<h1>Create PncItemCart</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>