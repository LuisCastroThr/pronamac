<?php
/* @var $this PncItemCartController */
/* @var $model PncItemCart */

$this->breadcrumbs=array(
	'Pnc Item Carts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PncItemCart', 'url'=>array('index')),
	array('label'=>'Create PncItemCart', 'url'=>array('create')),
	array('label'=>'View PncItemCart', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PncItemCart', 'url'=>array('admin')),
);
?>

<h1>Update PncItemCart <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>