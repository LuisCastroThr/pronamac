<?php
/* @var $this PncItemCartController */
/* @var $model PncItemCart */

$this->breadcrumbs=array(
	'Pnc Item Carts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PncItemCart', 'url'=>array('index')),
	array('label'=>'Create PncItemCart', 'url'=>array('create')),
	array('label'=>'Update PncItemCart', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PncItemCart', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PncItemCart', 'url'=>array('admin')),
);
?>

<h1>View PncItemCart #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_carrito',
		'id_producto',
		'cantidad',
		'monto',
		'create',
		'update',
	),
)); ?>
