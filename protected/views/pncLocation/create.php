<?php
/* @var $this PncLocationController */
/* @var $model PncLocation */

$this->breadcrumbs=array(
	'Pnc Locations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PncLocation', 'url'=>array('index')),
	array('label'=>'Manage PncLocation', 'url'=>array('admin')),
);
?>

<h1>Create PncLocation</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>