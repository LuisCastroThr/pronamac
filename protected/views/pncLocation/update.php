<?php
/* @var $this PncLocationController */
/* @var $model PncLocation */

$this->breadcrumbs=array(
	'Pnc Locations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PncLocation', 'url'=>array('index')),
	array('label'=>'Create PncLocation', 'url'=>array('create')),
	array('label'=>'View PncLocation', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PncLocation', 'url'=>array('admin')),
);
?>

<h1>Update PncLocation <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>