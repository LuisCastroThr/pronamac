<?php
/* @var $this PncLocationController */
/* @var $model PncLocation */

$this->breadcrumbs=array(
	'Pnc Locations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PncLocation', 'url'=>array('index')),
	array('label'=>'Create PncLocation', 'url'=>array('create')),
	array('label'=>'Update PncLocation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PncLocation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PncLocation', 'url'=>array('admin')),
);
?>

<h1>View PncLocation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_usuario',
		'calle',
		'ext_numero',
		'int_numero',
		'codigo_postal',
		'estado',
		'ciudad',
		'delegacion',
		'colonia',
		'telefono',
	),
)); ?>
