<?php
/* @var $this PncCategoriasController */
/* @var $model PncCategorias */

$this->breadcrumbs=array(
	'Pnc Categoriases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PncCategorias', 'url'=>array('index')),
	array('label'=>'Manage PncCategorias', 'url'=>array('admin')),
);
?>

<h1>Create PncCategorias</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>