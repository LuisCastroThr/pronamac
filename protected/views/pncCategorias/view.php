<?php
/* @var $this PncCategoriasController */
/* @var $model PncCategorias */

$this->breadcrumbs=array(
	'Pnc Categoriases'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PncCategorias', 'url'=>array('index')),
	array('label'=>'Create PncCategorias', 'url'=>array('create')),
	array('label'=>'Update PncCategorias', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PncCategorias', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PncCategorias', 'url'=>array('admin')),
);
?>

<h1>View PncCategorias #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
