<?php
/* @var $this PncCategoriasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pnc Categoriases',
);

$this->menu=array(
	array('label'=>'Create PncCategorias', 'url'=>array('create')),
	array('label'=>'Manage PncCategorias', 'url'=>array('admin')),
);
?>

<h1>Pnc Categoriases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
