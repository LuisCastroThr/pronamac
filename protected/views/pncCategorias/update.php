<?php
/* @var $this PncCategoriasController */
/* @var $model PncCategorias */

$this->breadcrumbs=array(
	'Pnc Categoriases'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PncCategorias', 'url'=>array('index')),
	array('label'=>'Create PncCategorias', 'url'=>array('create')),
	array('label'=>'View PncCategorias', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PncCategorias', 'url'=>array('admin')),
);
?>

<h1>Update PncCategorias <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>