<?php
/* @var $this PncProveedoresController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pnc Proveedores',
);

$this->menu=array(
	array('label'=>'Create PncProveedores', 'url'=>array('create')),
	array('label'=>'Manage PncProveedores', 'url'=>array('admin')),
);
?>

<h1>Pnc Proveedores</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
