<?php
/* @var $this PncProveedoresController */
/* @var $model PncProveedores */

$this->breadcrumbs=array(
	'Pnc Proveedores'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PncProveedores', 'url'=>array('index')),
	array('label'=>'Create PncProveedores', 'url'=>array('create')),
	array('label'=>'Update PncProveedores', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PncProveedores', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PncProveedores', 'url'=>array('admin')),
);
?>

<h1>View PncProveedores #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'descripcion',
		'direccion',
		'telefono',
		'rfc',
		'create',
	),
)); ?>
