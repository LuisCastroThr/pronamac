<?php
/* @var $this PncProveedoresController */
/* @var $model PncProveedores */

$this->breadcrumbs=array(
	'Pnc Proveedores'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PncProveedores', 'url'=>array('index')),
	array('label'=>'Create PncProveedores', 'url'=>array('create')),
	array('label'=>'View PncProveedores', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PncProveedores', 'url'=>array('admin')),
);
?>

<h1>Update PncProveedores <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>