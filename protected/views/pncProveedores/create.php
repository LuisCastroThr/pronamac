<?php
/* @var $this PncProveedoresController */
/* @var $model PncProveedores */

$this->breadcrumbs=array(
	'Pnc Proveedores'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PncProveedores', 'url'=>array('index')),
	array('label'=>'Manage PncProveedores', 'url'=>array('admin')),
);
?>

<h1>Create PncProveedores</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>