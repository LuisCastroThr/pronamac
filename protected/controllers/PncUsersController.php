<?php

class PncUsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','perfil','UpdatePerfil'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' 	 => array('admin','delete'),
				'users'		 => array('@'),
                'expression' => 'Yii::app()->user->id == Constants::User_type_admin'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PncUsers;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PncUsers']))
		{
			$model->attributes=$_POST['PncUsers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PncUsers']))
		{
			$model->attributes=$_POST['PncUsers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PncUsers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PncUsers('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PncUsers']))
			$model->attributes=$_GET['PncUsers'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PncUsers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PncUsers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PncUsers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pnc-users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionPerfil()
	{
		$id_user = Yii::app()->user->id;
		$user 	 = $this->loadModel($id_user);
		$location = PncLocation::model()->findByPk($id_user);

		$this->layout='main';
		$this->render('/perfil/perfil',array('user'=>$user, 'location' =>$location));
	}

	public function actionUpdatePerfil()
	{
		$data  		= array('error' => true);
		$id_user 	= Yii::app()->user->id;
		$flag 		= true;
		$changePass = false;

		if( isset($_POST['RegisterForm']) )
		{
			if( empty($_POST['RegisterForm']['name']) ){
				$data['errors']['name'] = 'Ingrese nombre.';
				$flag = false;
			}

			if( empty($_POST['RegisterForm']['lastname']) ){
				$data['errors']['lastname'] = 'Ingrese apellidos.';
				$flag = false;
			}

			if( !empty($_POST['RegisterForm']['password']) || !empty($_POST['RegisterForm']['repassword']) )
			{
				if( $_POST['RegisterForm']['password'] !== $_POST['RegisterForm']['repassword'] ){
					$data['errors']['password'] = 'No coincide la contraseña';
					$flag = false;
				}
				else
					$changePass = true;
			}

			//Registro exitoso
		 	if($flag){
		 		$user = PncUsers::model()->findByPk($id_user);
		 		$user->name 	= $_POST['RegisterForm']['name'];
		 		$user->lastname = $_POST['RegisterForm']['lastname'];
		 		if($changePass)
		 			$user->password = md5($_POST['RegisterForm']['password']);
		 		$user->save(false);

		 		$data = array(
		 			'success' => true,
		 			'message' => 'Datos actualizados correctamente.'
		 		);
		 	}
		}

		$this->layout = false;
        header('Content-type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
	}
}
