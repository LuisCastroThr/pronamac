<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionAboutUs()
	{
		$this->render('quienes-somos');
	}

	public function actionProducts()
	{
		$proveedor_id = Yii::app()->request->getParam('proveedor_id', null);
		$categoria_id = Yii::app()->request->getParam('categoria_id', null);
		$busqueda     = Yii::app()->request->getParam('term', null);
		$titleName 	  =  null;

		$criteria = new CDbCriteria();
		$criteria->select = 't.id, t.nombre, t.precio, t.image';
		$criteria->group = 't.nombre';
		$criteria->order = 't.image DESC';

		$criteria->addCondition('t.id_proveedor != 17'); //Descartamos a JOHNSON

		if(!empty($busqueda))
		{
			$criteria->addCondition('t.nombre like "%'.$busqueda.'%" OR t.codigo like "'.$busqueda.'" OR t.descripcion_gral like "'.$busqueda.'"');
		}
		else
		{
			if(!empty($proveedor_id)){
				$criteria->addCondition('t.id_proveedor = '.$proveedor_id);

				$proveedor = PncProveedores::model()->findByPk($proveedor_id);
				$titleName = $proveedor->name;
			}

			if(!empty($categoria_id)){
				$criteria->addCondition('t.id_categoria = '.$categoria_id);

				$categorias = PncCategorias::model()->findByPk($categoria_id);
				$titleName = $categorias->name;
			}
		}

		$data   = new CActiveDataProvider('PncProductos',
                                                    array
                                                    (
                                                        'criteria'    => $criteria,
                                                        'pagination'  => array
                                                        (
                                                            'pageSize' => 10
                                                        )
                                                    )
                                                );

		$this->render('productos', array('data' => $data, 'titleName' => $titleName));
	}

	public function actionArticles()
	{
		$this->render('bolsa-de-trabajo');
	}

	public function actionview_product()
	{
		$producto_id = Yii::app()->request->getParam('id', null);

		if( !empty($producto_id) ):
			$producto = PncProductos::model()->findByPk($producto_id);

			$presentaciones = PncProductos::model()->findAllByAttributes(
				array(
					'nombre' => $producto->nombre,
					'id_categoria' => $producto->id_categoria,
					'id_proveedor' => $producto->id_proveedor,
				)
			);

			$titleName = $producto->categoria->name;
			$titleProveedor = $producto->proveedor->name;

			$this->render('productos-view', array('presentaciones' => $presentaciones, 'titleName' => $titleName, 'titleProveedor' => $titleProveedor));
		else:
			throw new CHttpException(404,'');
		endif;
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contacto',array('model'=>$model));
	}



	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model = new LoginForm;
		$data  = array();

		if( isset($_POST['LoginForm']) )
		{
			$model->attributes = $_POST['LoginForm'];
			if( $model->validate() && $model->login() ){
				$data = array(
					'sucess' 	=> true,
					'redirect' 	=> Yii::app()->user->returnUrl
				);
			}
			else{
				$data['error'] = $model->getErrors();
			}
		}

		$this->layout = false;
        header('Content-type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionRegister()
	{
		$form = new RegisterForm;
		$data  = array();

		if( isset($_POST['RegisterForm']) )
		{
			$form->attributes 	= $_POST['RegisterForm'];
			$form->terminosCondiciones = ($form->terminosCondiciones == 'on') ? true : false;

			if( $form->validate() )
			{
				$user = PncUsers::model()->findByAttributes(array('email' => $form->email));
				if( empty($user) )
				{
					$user = new PncUsers();
					$user->attributes = $form->attributes;
					$user->password   = md5($user->password);
					if( $user->save(false) )
					{
						// Se hace login automático al registrarse
                        $login = new LoginForm;
                        $login->username = $form->email;
                        $login->password = $form->password;

                        // validar usuario y hacer login si es exitoso
                        if($login->validate() && $login->login())
                        {
							$data = array(
								'sucess' 	=> true,
								'redirect'  => Yii::app()->createUrl('pncUsers/perfil')
							);
						}
						else
							$data['error'] = $login->getErrors();
					}
				}
				else
					$data['error'] = 'El usuario ya existe.';
			}
			else
				$data['error'] = $form->getErrors();
		}

		$this->layout = false;
        header('Content-type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
	}

	public function actionCateteres()
	{
		$this->render('./templates/articulos/cateteres');
	}

	public function actionHeridas()
	{
		$this->render('./templates/articulos/cuidados-de-heridas');
	}

	public function actionBacteriemia()
	{
		$this->render('./templates/articulos/bacteriemia-cero');
	}

	public function actionCorazon()
	{
		$this->render('./templates/articulos/cuidado-para-tu-corazon');
	}

	public function actionBotiquin()
	{
		$this->render('./templates/articulos/elementos-esenciales-de-un-botiquin');
	}

	public function actionCuracion()
	{
		$this->render('./templates/articulos/material-de-curacion');
	}

	public function actionEsponja()
	{
		$this->render('./templates/articulos/esponja-esteril-con-sulfato-de-gentamicina-y-colageno');
	}

	public function actionContactar()
	{
		$form = new ContactForm;
		$data  = array();

		if( isset($_POST['ContactForm']) )
		{
			$form->attributes 	= $_POST['ContactForm'];
			if( $form->validate() )
			{
				$subject = "Contacto Pronamac.";
				if( Tools::sendEmail($form->name, $subject, $form->body, $form->email) )
				{
					$data = array(
						'sucess' 	=> true,
					);
				}
				else
					$data['error'] = 'No se pudo enviar el correo.';
			}
			else
				$data['error'] = $form->getErrors();
		}

		$this->layout = false;
        header('Content-type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
	}

	public function actionPdf()
	{
		$this->layout = '//layouts/pdf';
		$this->render('pdf-generator');
	}

	public function actionTerminosCondiciones()
	{
		$this->render('terminos-condiciones');
	}


	public function actionAvisoPrivacidad()
	{
		$this->render('aviso-privacidad');
	}
	
	public function actionPoliticasEnvio()
	{
		$this->render('politicas-envio');
	}	
}
