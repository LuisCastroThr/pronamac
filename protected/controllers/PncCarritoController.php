<?php

class PncCarritoController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('AddToCart','ViewCart','captcha','DeleteItem', 'UpdateCart','CartStepTwo', 'NumberAccount','SendPay'),
				'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PncCarrito;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PncCarrito']))
		{
			$model->attributes=$_POST['PncCarrito'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PncCarrito']))
		{
			$model->attributes=$_POST['PncCarrito'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PncCarrito');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PncCarrito('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PncCarrito']))
			$model->attributes=$_GET['PncCarrito'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PncCarrito the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PncCarrito::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PncCarrito $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pnc-carrito-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionAddToCart()
	{
		$productos = Yii::app()->request->getParam('productos', null);
		$column    = Yii::app()->user->isGuest ? 'fingerprint' : 'id_user';
		$value     = Yii::app()->user->isGuest ? Tools::getFingerprint() : Yii::app()->user->id;

		$response  	= array();
		$id_carrito = null;
		//Valida que vengan productos en la peticion
		if( !empty($productos) ):
			//Verifica si ya existe un carrito del usuario
			$carrito = PncCarrito::model()->findByAttributes(array($column => $value, 'status' => Constants::Cart_active));
			if( empty($carrito) ):
				$carrito = new PncCarrito();
				$carrito->id_user 		= Yii::app()->user->id;
				$carrito->fingerprint 	= Tools::getFingerprint();
				$carrito->status 	    = Constants::Cart_active;
				if( $carrito->save(false) ):
					$id_carrito = $carrito->id;
				endif;
			endif;

			//Obtenemos ID del carrito vigente
			$id_carrito = $carrito->id;

			//Guarda detalle de compras
			foreach ($productos as $key => $value):
				$item = new PncItemCart();
				$item->id_carrito 	= $id_carrito;
				$item->id_producto 	= $key;
				$item->cantidad 	= $value;
				$item->monto 	    = Tools::getAmount($key, $value);
				$item->create 	    = date('Y-m-d H:m:s');
				if( $item->save() ):
					$itemCart = Tools::getDetailCart($id_carrito);
					$response = array(
						'success'  => true,
						'itemCart' => $itemCart,
						'totalItem' => count($itemCart)
					);
				else:
					$response['error'] = $item->getErrors();
				endif;
			endforeach;
		else:
			$response['error'] = 'Seleccione un producto.';
		endif;

		$this->layout = false;
        header('Content-type: application/json');
        echo  CJSON::encode($response);
        Yii::app()->end();
	}

	public function actionViewCart()
	{
		$itemCart = array();
		$detalles = array(
			'subTotal' 	=> 0,
			'envio' 	=> 0,
			'iva' 		=> 0,
			'total' 	=> 0,
		);

		Tools::UpdateUserSession( Tools::getFingerprint() );

		$carrito = PncCarrito::model()->findByAttributes(
						array(
							'id_user' => Yii::app()->user->id,
							'status'  => Constants::Cart_active
						)
					);

		if( !empty($carrito) ):
			$itemCart = Tools::getDetailCart($carrito->id);
			$detalles = Tools::DetalleCarrito($carrito->id);
		endif;

		$this->layout = 'main';
		$this->render('/cart/carrito',
			array(
				'itemCart' => $itemCart,
				'detalle'  => $detalles
			)
		);
	}

	public function actionDeleteItem()
	{
		$id_item 	= Yii::app()->request->getParam('id_item', null);
		$response 	= array();

		if(!empty($id_item))
		{
			$itemDelete = PncItemCart::model()->findByPk($id_item);
			if( $itemDelete->delete() == true )
			{
				$response = array(
					'success' => true,
					'messages' => 'El producto se borro correctamente.'
				);
			}
		}

		$this->layout = false;
        header('Content-type: application/json');
        echo  CJSON::encode($response);
        Yii::app()->end();
	}

	public function actionUpdateCart()
	{
		$response   = array();
		$iditem 	= Yii::app()->request->getParam('item', null);
		$cantidad 	= Yii::app()->request->getParam('cantidad', null);

		if(!empty($iditem) && !empty($cantidad)){
			$response = Tools::updateCarrito($iditem,$cantidad);
		}else{
			$response['error'] = true;
		}

		$this->layout = false;
        header('Content-type: application/json');
        echo  CJSON::encode($response);
        Yii::app()->end();
	}

	public function actionCartStepTwo()
	{
		$id_user 	= Yii::app()->user->id;
		$location = PncLocation::model()->findByPk($id_user);
		$factura = PncFacturas::model()->findByPk($id_user);

		$carrito = PncCarrito::model()->findByAttributes(
						array(
							'id_user' => Yii::app()->user->id,
							'status'  => Constants::Cart_active
						)
					);

		$this->layout = '//layouts/main';
		$this->render('/cart/carrito-step2',
			array(
				'detalle'  => Tools::DetalleCarrito($carrito->id),
				'location' => $location,
				'factura' => $factura,
			)
		);
	}

	public function actionNumberAccount()
	{
		$id_user 	= Yii::app()->user->id;
		$id_carrito = Yii::app()->request->getParam('cart_id', null);

		if(!empty($id_carrito)):
			$factura = PncFacturas::model()->findByPk($id_user);
			$carrito = PncCarrito::model()->findByAttributes(
							array(
								'id_user' => Yii::app()->user->id,
								'id'      => $id_carrito,
								'status'  => Constants::Cart_payment_pending
							)
						);

			$this->render('/cart/number-account',
				array(
					'carrito'  => $carrito,
					'factura'  => $factura,
					'referencia'  => $carrito->id
				)
			);
		else:
			$this->render('/cart/select_cart');
		endif;
	}

	public function actionSendPay()
	{
		$location = Yii::app()->request->getParam('location', null);
		$invoice  = Yii::app()->request->getParam('invoice', null);
		$response = array();

		if(!empty($location) && !empty($invoice)):
			$carrito = PncCarrito::model()->findByAttributes(
							array(
								'id_user' => Yii::app()->user->id,
								'status'  => Constants::Cart_active
							)
						);
			if(!empty($carrito )):
				//Detalles del carrito final
				$detalles = Tools::DetalleCarrito($carrito->id);
				
				$carrito->type_send = $location;
				$carrito->invoice  	= $invoice;
				$carrito->total  	= $detalles['total_num'];
				$carrito->status  	= Constants::Cart_payment_pending;
				$carrito->update  	= date('Y-m-d H:m:s');;
				$carrito->save(false);

				$response = array('success' => true, 'cart_id' => $carrito->id);
			else:
				$response = array('error' => true, 'message' => 'No existe el carrito.');
			endif;
		endif;

		$this->layout = false;
        header('Content-type: application/json');
        echo  CJSON::encode($response);
        Yii::app()->end();
	}
}
