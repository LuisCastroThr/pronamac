<?php 

class Tools 
{

    private function Tools() 
    { 
        /* Evita que se instancie */ 
    }

    public static function generateFingerPrint()
    {
        $usable_separators = array(',', '.', '|', '/', '&', '#', '-', '_', '+', '·');
        $microtime         = microtime(true);
        $usable_separator  = $usable_separators[substr($microtime, -1)];
        $server            = implode($usable_separator, $_SERVER);
        $data              = $server.$usable_separator.$microtime.$usable_separator.openssl_random_pseudo_bytes(rand(20, 40)).$usable_separator.uniqid();
        $hash              = hash('sha512', $data);

        return hash('sha512', $data);
    }

    public static function setFingerprintCookie($hash)
    {
        $cookie = new CHttpCookie('user_fingerprint', $hash);
        Yii::app()->request->cookies['user_fingerprint'] = $cookie;
    }

    public static function registerNewFingerprint()
    {
        $hash = Tools::generateFingerPrint();
        Tools::setFingerprintCookie($hash);
        return $hash;
    }

    public static function recoverFingerprintCookie()
    {
        return Yii::app()->request->cookies['user_fingerprint']->value;
    }

    public static function getFingerprint()
    {
        if(!isset(Yii::app()->request->cookies['user_fingerprint']) || Tools::recoverFingerprintCookie() == null)
        {
            return Tools::registerNewFingerprint();
        }

        return Tools::recoverFingerprintCookie();
    }

    public static function getAmount($id_producto, $cantidad)
    {
        $monto      = 0;
        $producto   = PncProductos::model()->findByPk($id_producto);
        
        if( !empty($producto) ):
            $monto = $cantidad * $producto->precio;
            $monto = round($monto, 2);
        endif;

        return $monto;
    }

    public static function getDetailCart($id_carrito)
    {
        $itemCart = array();

        $criteria = new CDbCriteria();
        $criteria->select = 't.id, t.id_producto, t.cantidad, t.monto';
        $criteria->addCondition('t.id_carrito = '.$id_carrito);
        $cartItems = PncItemCart::model()->findAll($criteria);
        
        foreach ($cartItems as $item):
            $itemCart[$item->id] = array(
                'image'     => $item->producto->image,
                'producto'  => $item->producto->nombre,
                'presenta'  => $item->producto->presentacion,
                'precio'    => $item->producto->precio,
                'cantidad'  => $item->cantidad,
                'total'     => $item->monto,
                'id'        => $item->id
            );
        endforeach;

        return $itemCart;
    }

    /**
    * Envía correo electrónico
    * @param $subject El asunto
    * @param $body El cuerpo
    * @param $email La dirección del destinatario
    * @param $attachment Archivos adjuntos
    * @return bool true si se envío el correo; false si falló
    */
    public static function sendEmail($name, $subject, $body, $email, $Attachment = null) {
        
        $response = false;

        Yii::import('application.extensions.phpMailer.yiiPhpMailer');
        
        $mail = new yiiPhpMailer;
        $mail->SMTPSecure = 'ssl';
        //$mail->IsSMTP();
        $mail->Host     = Yii::app()->params['mailHost'];
        $mail->SMTPAuth = Yii::app()->params['mailSMTPAuth'];
        $mail->Username = Yii::app()->params['mailUsername'];
        $mail->Password = Yii::app()->params['mailPassword'];
        $mail->SetFrom(Yii::app()->params['mailSenderEmail'], Yii::app()->params['mailSenderName']);
        $mail->Subject  = $subject;
        $mail->MsgHTML($body);
        $mail->AddAddress($email, $name);
        $mail->CharSet = 'utf-8';  
        $mail->Mailer = "smtp"; 
        if( $mail->Send() )
             $response = true;

        return $response;
    }

    //Actualiza el USER ID de la tabla de carrito  si existen productos
    //Condicionando el fingerPrint
    public static function UpdateUserSession($fingerprint)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.fingerprint = "'.$fingerprint.'"');
        $criteria->addCondition('t.id_user IS NULL');

        $carrito = PncCarrito::model()->findAll($criteria);
        if( !empty($carrito) )
        {
            foreach ($carrito as $carr) {
                $carr->id_user = Yii::app()->user->id;
                $carr->save(false);
            }
        }
    }

    public static function getTotalCart()
    {
        $total  = 0;
        $carrito = PncCarrito::model()->findByAttributes(
                        array(
                            'id_user' => Yii::app()->user->id,
                            'status'  => Constants::Cart_active
                        )
                    );

        if(!empty($carrito))
        {
            $criteria               = new CDbCriteria();
            $criteria->select = 't.id';
            $criteria->addCondition('t.id_carrito = '.$carrito->id);
            $total = PncItemCart::model()->count($criteria);
        }

        return $total;
    }

    public static function updateCarrito($iditem,$cantidad, $destino = null)
    {
        $carrito = array();

        $item = PncItemCart::model()->findByPk($iditem);
        $item->cantidad     = $cantidad;
        $item->monto        = Tools::getAmount($item->producto->id, $cantidad);
        $item->update       = date('Y-m-d H:m:s');
        if( $item->save() ):
            $carrito['success'] = true;
            $carrito['detalle'] = Tools::DetalleCarrito($item->id_carrito);
            $carrito['item'] = array(
                'id' => $item->id,
                'monto'   => Tools::formatPrice($item->monto)
            );
        endif;

        return $carrito;
    }

    public static function formatPrice($price)
    {
        return number_format($price,2);
    }

    public static function DetalleCarrito($id_carrito)
    {
        $subTotal = 0;
        $total    = 0;
        $iva      = 0;
        $detalle  = array();

        $carrito = PncCarrito::model()->findByPk($id_carrito);
        $items   = PncItemCart::model()->findAllByAttributes(array('id_carrito' => $id_carrito));
        
        foreach ($items as $item) {
            $subTotal += $item->monto;
        }

        $iva    = $subTotal * .16;
        $total  = $subTotal + $iva;

        $detalle = array(
            'subTotal'  => Tools::formatPrice($subTotal),
            'iva'       => Tools::formatPrice($iva),
            'total'     => Tools::formatPrice($total),
            'total_num' => $total,
            'envio'     => 0
        );


        return $detalle;
    }

    public static function getCategorias($str_categoria)
    {
        $id_vategoria = null;

        $str_categoria = trim($str_categoria);
        $str_categoria = str_repeat(' ', '', $str_categoria);

        switch ($str_categoria) {
            case 'materialdecuración':
                $id_vategoria = 1;
                break;
            case 'cuidadoavanzadodeheridas':
            case 'cuidadodeheridas':
                $id_vategoria = 2;
                break;
            case 'hipodérmicos':
                $id_vategoria = 4;
                break;
            case 'ostomias':
                $id_vategoria = 6;
                break;
            case 'prevencióndeinfecciones':
                $id_vategoria = 7;
                break;
            case 'otros':
            case '':
                $id_vategoria = 8;
                break;
            case 'quirúrgico':
                $id_vategoria = 9;
                break;
            default:
                echo $str_categoria; 
                break;
        }
        return $id_vategoria;
    }
}