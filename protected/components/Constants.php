<?php

class Constants
{
	//Tipo de usuarios
	const User_type_normal = 0;
	const User_type_admin  = 1;

	//Estados del carrito de compras
	const Cart_active            = 0;
    const Cart_payment_pending   = 1;
    const Cart_payment_completed = 2;
    const Cart_cancel            = 3;

    //Categorias
    const Cat_materia_curacion 	= 1;
    const Cat_cuidado_heridas 	= 2;
    const Cat_hipodermicos 		= 4;
    const Cat_ostomias 			= 6;
    const Cat_prevencion_infecc = 7;
    const Cat_otros 			= 8;
    const Cat_quirurgico 		= 9;

    public static $states = array
    (
        1  => 'Cdmx',
        2  => 'Aguascalientes',
        3  => 'Baja California Norte',
        4  => 'Baja California Sur',
        5  => 'Campeche',
        6  => 'Coahuila de Zaragoza',
        7  => 'Colima',
        8  => 'Chiapas',
        9  => 'Chihuahua',
        10 => 'Durango',
        11 => 'Guanajuato',
        12 => 'Guerrero',
        13 => 'Hidalgo',
        14 => 'Jalisco',
        15 => 'Estado de México',
        16 => 'Michoacán de Ocampo',
        17 => 'Morelos',
        18 => 'Nayarit',
        19 => 'Nuevo León',
        20 => 'Oaxaca',
        21 => 'Puebla',
        22 => 'Querétaro',
        23 => 'Quintana Roo',
        24 => 'San Luis Potosí',
        25 => 'Sinaloa',
        26 => 'Sonora',
        27 => 'Tabasco',
        28 => 'Tamaulipas',
        29 => 'Tlaxcala',
        30 => 'Veracruz',
        31 => 'Yucatán',
        32 => 'Zacatecas'
    );

    public static $factura_tipo = array(
        1 => 'Persona moral',
        2 => 'Persona física',
    );

    //Opciones de envio
    const opt_owner_user = 1;
    const opt_sucur_cdmx = 2;
    const opt_sucur_jalisco = 3;
    const opt_sucur_monterrey = 4;
}