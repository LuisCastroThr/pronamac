<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$criteria = new CDbCriteria();
        $criteria->condition = 'LOWER(email) = :email AND password = :pass';
        $criteria->params = array(
            ':email'=> strtolower($this->username),
            ':pass' => md5($this->password)
        );
        $users = PncUsers::model()->find($criteria);

		if($users === null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else{
			$this->setState('id',$users->id);
			$this->setState('typeUser',$users->type_user);
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
}