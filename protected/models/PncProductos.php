<?php

/**
 * This is the model class for table "pnc_productos".
 *
 * The followings are the available columns in table 'pnc_productos':
 * @property integer $id
 * @property string $codigo
 * @property string $nombre
 * @property string $descripcion_gral
 * @property string $descripcion_pres
 * @property string $presentacion
 * @property string $piezas_x_unidad
 * @property integer $unidad
 * @property integer $id_categoria
 * @property integer $id_proveedor
 * @property string $precio
 * @property string $image
 * @property string $pdf
 * @property string $create
 */
class PncProductos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pnc_productos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, id_categoria, id_proveedor, precio, create', 'required'),
			array('id_categoria, id_proveedor', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>100),
			array('nombre, image, pdf', 'length', 'max'=>200),
			array('presentacion, piezas_x_unidad', 'length', 'max'=>500),
			array('precio', 'length', 'max'=>20),
			array('descripcion_gral, descripcion_pres', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo, nombre, descripcion_gral, descripcion_pres, presentacion, piezas_x_unidad, unidad, id_categoria, id_proveedor, precio, image, pdf, create', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proveedor' => array(self::BELONGS_TO, 'PncProveedores', 'id_proveedor'),
			'categoria' => array(self::BELONGS_TO, 'PncCategorias', 'id_categoria'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'nombre' => 'Nombre',
			'descripcion_gral' => 'Descripcion Gral',
			'descripcion_pres' => 'Descripcion Pres',
			'presentacion' => 'Presentacion',
			'piezas_x_unidad' => 'Piezas X Unidad',
			'unidad' => 'Unidad',
			'id_categoria' => 'Id Categoria',
			'id_proveedor' => 'Id Proveedor',
			'precio' => 'Precio',
			'image' => 'Image',
			'pdf' => 'Pdf',
			'create' => 'Create',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion_gral',$this->descripcion_gral,true);
		$criteria->compare('descripcion_pres',$this->descripcion_pres,true);
		$criteria->compare('presentacion',$this->presentacion,true);
		$criteria->compare('piezas_x_unidad',$this->piezas_x_unidad,true);
		$criteria->compare('unidad',$this->unidad);
		$criteria->compare('id_categoria',$this->id_categoria);
		$criteria->compare('id_proveedor',$this->id_proveedor);
		$criteria->compare('precio',$this->precio,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('pdf',$this->pdf,true);
		$criteria->compare('create',$this->create,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PncProductos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
