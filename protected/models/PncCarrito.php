<?php

/**
 * This is the model class for table "pnc_carrito".
 *
 * The followings are the available columns in table 'pnc_carrito':
 * @property integer $id
 * @property integer $id_producto
 * @property integer $id_user
 * @property string $fingerprint
 * @property integer $cantidad
 * @property string $total
 * @property integer $status
 * @property string $create
 * @property string $update
 */
class PncCarrito extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pnc_carrito';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_producto, cantidad, total, status, create', 'required'),
			array('id_producto, id_user, cantidad, status', 'numerical', 'integerOnly'=>true),
			array('fingerprint', 'length', 'max'=>150),
			array('total', 'length', 'max'=>20),
			array('update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_producto, id_user, fingerprint, cantidad, total, status, create, update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_producto' => 'Id Producto',
			'id_user' => 'Id User',
			'fingerprint' => 'Fingerprint',
			'cantidad' => 'Cantidad',
			'total' => 'Total',
			'status' => 'Status',
			'create' => 'Create',
			'update' => 'Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('fingerprint',$this->fingerprint,true);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create',$this->create,true);
		$criteria->compare('update',$this->update,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PncCarrito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
