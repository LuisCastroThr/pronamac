<?php

/**
 * This is the model class for table "pnc_location".
 *
 * The followings are the available columns in table 'pnc_location':
 * @property integer $id
 * @property integer $id_usuario
 * @property string $calle
 * @property string $ext_numero
 * @property string $int_numero
 * @property string $codigo_postal
 * @property string $estado
 * @property string $ciudad
 * @property string $delegacion
 * @property string $colonia
 * @property string $telefono
 */
class PncLocation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pnc_location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_usuario, codigo_postal, estado, ciudad, delegacion, colonia', 'required'),
			array('id_usuario', 'numerical', 'integerOnly'=>true),
			array('codigo_postal', 'length', 'max'=>10),
			array('estado, ciudad, delegacion, colonia', 'length', 'max'=>100),
			array('calle, ext_numero, int_numero, telefono', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_usuario, calle, ext_numero, int_numero, codigo_postal, estado, ciudad, delegacion, colonia, telefono', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_usuario' => 'Id Usuario',
			'calle' => 'Calle',
			'ext_numero' => 'Ext Numero',
			'int_numero' => 'Int Numero',
			'codigo_postal' => 'Codigo Postal',
			'estado' => 'Estado',
			'ciudad' => 'Ciudad',
			'delegacion' => 'Delegacion',
			'colonia' => 'Colonia',
			'telefono' => 'Telefono',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_usuario',$this->id_usuario);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('ext_numero',$this->ext_numero,true);
		$criteria->compare('int_numero',$this->int_numero,true);
		$criteria->compare('codigo_postal',$this->codigo_postal,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('ciudad',$this->ciudad,true);
		$criteria->compare('delegacion',$this->delegacion,true);
		$criteria->compare('colonia',$this->colonia,true);
		$criteria->compare('telefono',$this->telefono,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PncLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
