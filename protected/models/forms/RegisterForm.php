<?php

class RegisterForm extends CFormModel
{
	public $name;
	public $email;
	public $password;
	public $repassword;
	public $terminosCondiciones;
	public $lastname;
	
	public function rules()
	{
		return array(
			array('name, email, password, repassword', 'required'),
			array('name, email', 'length', 'max'=>100),
			array('password', 'length', 'max'=>200),
			array('terminosCondiciones', 'required','message'=>'Debe aceptar los terminos y condiciones.'),
			array('terminosCondiciones', 'boolean'),
			array('password','compare','compareAttribute'=>'repassword','message'=>'La contraseña de acceso no es la misma.'),
			array('email','unique', 'className' => 'PncUsers', 'message'=>'Utilice otro email para registrarse.'),
		);
	}
}
