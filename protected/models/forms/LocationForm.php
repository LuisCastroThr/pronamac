<?php

class LocationForm extends CFormModel
{
	public $calle;
	public $ext_numero;
	public $int_numero;
	public $codigo_postal;
	public $estado;
	public $ciudad;
	public $delegacion;
	public $colonia;
	public $telefono;
	
	public function rules()
	{
		return array(
			array('calle, codigo_postal, estado, ext_numero, colonia', 'required'),
			array('calle, ext_numero, int_numero, ciudad, delegacion, colonia, telefono', 'length', 'max'=>200),
		);
	}
}
