<?php

class FacturasForm extends CFormModel
{
	public $razon;
	public $rfc;
	public $tipo;
	public $correo;
	public $calle;
	public $ext_numero;
	public $int_numero;
	public $codigo_postal;
	public $estado;
	public $ciudad;
	public $delegacion;
	public $colonia;
	public $telefono;
	
	public function rules()
	{
		return array(
			array('calle, codigo_postal, estado, ext_numero, colonia, razon, rfc, tipo', 'required'),
			array('calle, ext_numero, int_numero, ciudad, delegacion, colonia, telefono, correo', 'length', 'max'=>200),
		);
	}
}
