<?php

/**
 * This is the model class for table "pnc_item_cart".
 *
 * The followings are the available columns in table 'pnc_item_cart':
 * @property integer $id
 * @property integer $id_carrito
 * @property integer $id_producto
 * @property integer $cantidad
 * @property string $monto
 * @property string $create
 * @property string $update
 */
class PncItemCart extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pnc_item_cart';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_carrito, id_producto, cantidad, monto, create', 'required'),
			array('id_carrito, id_producto, cantidad', 'numerical', 'integerOnly'=>true),
			array('monto', 'length', 'max'=>20),
			array('update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_carrito, id_producto, cantidad, monto, create, update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'producto' => array(self::BELONGS_TO, 'PncProductos', 'id_producto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_carrito' => 'Id Carrito',
			'id_producto' => 'Id Producto',
			'cantidad' => 'Cantidad',
			'monto' => 'Monto',
			'create' => 'Create',
			'update' => 'Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_carrito',$this->id_carrito);
		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('monto',$this->monto,true);
		$criteria->compare('create',$this->create,true);
		$criteria->compare('update',$this->update,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PncItemCart the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
