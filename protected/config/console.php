<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Pronamac Application Console',

	// preloading 'log' component
	'preload'=>array('log'),

	'import'   => array
    (
    	'application.models.*',
        'application.extensions.phpexcelreader.*',
    ),

	// application components
	'components'=>array(

		// database settings are configured in database.php
		'db' =>  array
        (
            //Cambiar credenciales
            'class'                 => 'CDbConnection',
            'connectionString'      => 'mysql:host=localhost;port=3306;dbname=Pronamac',
            'username'              => 'root',
            'password'              => 'luisthr',
            'charset'               => 'utf8',
        ),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

	),
	'params'=>array
    (
    	'rutaData'   => '/propiedades/pronamac/protected/data/',
    )
);
