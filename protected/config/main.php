<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'	=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'		=> 'My Web Application',
	'theme'		=> 'classic',

	// preloading 'log' component
	'preload'	=> array('log'),

	// autoloading model and component classes
	'import'	=> array(
		'application.models.*',
		'application.models.forms.*',
		'application.components.*',
	),
	'modules' 	=> array(
		// uncomment the following to enable the Gii tool
		'gii'	=> array(
			'class'		=> 'system.gii.GiiModule',
			'password' 	=> 'AdminPr0namaC',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'	=> array($_SERVER['REMOTE_ADDR']),
		),
	),

	// application components
	'components'	=> array(
		'user' 	=> array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'urlSuffix' => '.php',
			'rules'=>array(

				'quienes-somos' => 'site/AboutUs',
				'productos' 	=> 'site/Products',
				'articulos' 	=> 'site/Articles',
				'contacto' 		=> 'site/Contact',
				'ver-carrito'   => 'pncCarrito/ViewCart',
				'envio-carrito' => 'pncCarrito/CartStepTwo',
				'list' 	        => 'site/list',
				'pdf' 	        => 'site/pdf',
				'producto' 	    => 'site/view_product',
				'number-account'   => 'pncCarrito/NumberAccount',


				//Productos por categoria
				'prevencion-infecciones-<categoria_id>' 	 => 'site/Products',
				'cuidado-avanzado-de-heridas-<categoria_id>' => 'site/Products',
				'material-de-curacion-<categoria_id>' 		 => 'site/Products',
				'ostomias-<categoria_id>' 					 => 'site/Products',
				'hipodermicos-<categoria_id>' 				 => 'site/Products',
				'quirurgico-<categoria_id>' 				 => 'site/Products',
				'otros-<categoria_id>' 						 => 'site/Products',

				//Articulos
				'cateteres'     						=> 'site/cateteres',
				'cuidados-de-heridas'  					=> 'site/heridas',
				'bacteriemia-cero'  					=> 'site/bacteriemia',
				'cuidado-para-tu-corazon'  				=> 'site/corazon',
				'elementos-esenciales-de-un-botiquin'  	=> 'site/botiquin',
				'material-de-curacion'  				=> 'site/curacion',
				'esponja-esteril-con-sulfato-de-gentamicina-y-colageno'  => 'site/esponja',


				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'db' =>  array
        (
            //Cambiar credenciales
            'class'                 => 'CDbConnection',
            'connectionString'      => 'mysql:host=localhost;port=3306;dbname=Pronamac',
            'username'              => 'root',
            'password'              => '',
            'charset'               => 'utf8',
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),
	'params'=>array(
		//'url_test' 	=> '/pronamac_new',
		// Cambiar configuración de correo
        'mailSenderEmail'         => 'no-reply@propiedades.com',
        'mailSenderName'          => 'Propiedades.com',
        'mailHost'                => 'smtp.gmail.com',
        'mailSMTPAuth'            => true,
        'mailUsername'            => 'no-reply@propiedades.com',
        'mailPassword'            => 'MK7648JHK6',
        'smtpError'               => 'Error al enviar el email.',
	),
);
