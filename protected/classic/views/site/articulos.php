<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Artículos</h1>
		<p>Ponemos a tu alcance artículos de consulta  por expertos en el tema de salud, que ayudaran a tener una explicación exacta para lo que tu necesitas.</p>
		<br>

		<div class="m-b row column">
			<h2 class="h2-text">Catéteres</h2>
			<p>De acuerdo a la <strong>localización</strong> anatómica se denominan: Catéter venoso periférico (CVP), catéter venoso periférico de línea media (CVPM), catéter central de inserción periférica (PICC) y catéter venoso central (CVC).</p>
			<a class="float-right" href="cateteres.php">Leer más</a>
		</div>

		<div class="m-b row column">
			<h2 class="h2-text">Cuidado de Heridas</h2>
			<p>Antes de saber qué hacer para que una herida no se infecte es necesario conocer qué es la <strong>infección de una herida</strong>. Una infección de herida ocurre cuando los microbios se instalan en la incisión de la piel. Estas bacterias se alojan en los tejidos impidiendo que la herida se cure y causando otros síntomas.</p>
			<a class="float-right" href="cuidados-de-heridas.php">Leer más</a>
		</div>

		<div class="m-b row column">
			<h2 class="h2-text">Bacteriemia Cero</h2>
			<p>El uso de dispositivos intravenosos incrementa el riesgo para el paciente de contraer infecciones dentro del mismo hospital si no se cumplen las medidas preventivas para su colocación y/o las condiciones de salud no son las óptimas. </p>
			<a class="float-right" href="bacteriemia-cero.php">Leer más</a>
		</div>

		<div class="m-b row column">
			<h2 class="h2-text">Cuida a tu Corazón</h2>
			<p>Te mostramos algunas acciones que puedes llevar a cabo para cuidar tu corazón. ¡No lo dejes para mañana!</p>
			<a class="float-right" href="cuidado-para-tu-corazon.php">Leer más</a>
		</div>

		<div class="m-b row column">
			<h2 class="h2-text">Elementos Esenciales de un Botiquín</h2>
			<p>Los elementos esenciales de un botiquín de primeros auxilios se pueden clasificar en antisépticos, instrumental y elementos adicionales, material de curación y medicamentos.</p>
			<a class="float-right" href="elementos-esenciales-de-un-botiquin.php">Leer más</a>
		</div>

		<div class="m-b row column">
			<h2 class="h2-text">Material de Curación</h2>
			<p>El <strong>material de curación</strong> son aquellos instrumentos y productos que se utilizan para curar a una persona antes, durante o después de sufrir una herida.</p>
			<a class="float-right" href="material-de-curacion.php">Leer más</a>
		</div>

		<div class="m-b row column">
			<h2 class="h2-text">Esponja estéril con sulfato de gentamicina y colágeno</h2>
			<p>La <strong>esponja de colágeno bovino con gentamicina</strong> es una combinación de dos sustancias para la aplicación local de antibióticos teniendo un efecto hemostático promoviendo la cicatrización y provocando altas concentraciones locales del antibiótico en el sitio de heridas quirúrgicas o traumáticas.</p>
			<a class="float-right" href="esponja-esteril-con-sulfato-de-gentamicina-y-colageno.php">Leer más</a>
		</div>
	</div>
</div>