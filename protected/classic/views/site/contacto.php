<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">

		<h1 class="h1-text">Contacto</h1>

		<div class="row m-b">
			<div class="small-5 large-5 columns">
				<h2 class="h2-text">Matriz Ciudad de México</h2>
				<p>Pronamac, S.A. de C.V.<br>5 de Febrero N° 809, Col. Álamos, Delegación Benito Juárez, C.P. 03400 Ciudad de México</p>
				<p>Teléfonos: (55) 55795400, 55793390, 56961551</p>
				<p>Fax: (55) 5579 2811</p>

			</div>
			<div class="small-7 large-7 columns">
				<iframe src="https://maps.google.com.mx/maps/ms?msa=0&amp;msid=201896345870942678455.0004d3e30e36320341ab0&amp;ie=UTF8&amp;t=m&amp;ll=19.393663,-99.140775&amp;spn=0.004858,0.008562&amp;z=16&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="400" height="240"></iframe>
				<br>
				<small>Ver <a href="https://maps.google.com.mx/maps/ms?msa=0&amp;msid=201896345870942678455.0004d3e30e36320341ab0&amp;ie=UTF8&amp;t=m&amp;ll=19.393663,-99.140775&amp;spn=0.004858,0.008562&amp;z=16&amp;source=embed">Pronamac</a> en un mapa ampliado</small>
			</div>
		</div>

		<div class="row m-b">
			<div class="small-5 large-5 columns">
				<h2 class="h2-text">Sucursal Zapopan, Jalisco</h2>
				<p>Pronamac de Occidente<br> Giosue Carducci No.5541 Col. Jardines Vallarta, C.P. 45150 Zapopan, Jalisco</p>
				<p>Teléfonos: 01 (33) 1809 9540</p>

			</div>
			<div class="small-7 large-7 columns">
				<iframe src="https://maps.google.com.mx/maps/ms?msa=0&amp;msid=201896345870942678455.0004d3e380640855422c1&amp;ie=UTF8&amp;t=m&amp;ll=20.651459,-103.415551&amp;spn=0.004618,0.00912&amp;z=16&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="400" height="240"></iframe>
				<br>
				<small>Ver <a href="https://maps.google.com.mx/maps/ms?msa=0&amp;msid=201896345870942678455.0004d3e380640855422c1&amp;ie=UTF8&amp;t=m&amp;ll=20.651459,-103.415551&amp;spn=0.004618,0.00912&amp;z=16&amp;source=embed">Pronamac de Occidente</a> en un mapa ampliado</small>
			</div>
		</div>

		<!-- <div class="row">
			<div class="small-5 large-5 columns">
				<h2 class="h2-text">Filial - León, Guanajuato</h2>
				<p>Promehbsa, S.A. de C.V. <br> Blvd. Tepeyac No. 117 Col. León Moderno C.P. 37480 León Gto.</p>
				<p>Teléfonos: 01 (477) 770-60-02</p>
				<p>Fax:  770-64-24</p>
			</div>
			<div class="small-7 large-7 columns">
				<iframe src="https://maps.google.com.mx/maps/ms?msa=0&amp;msid=201896345870942678455.0004d484579947894e95b&amp;ie=UTF8&amp;t=m&amp;ll=21.108964,-101.670506&amp;spn=0.004804,0.008562&amp;z=16&amp;output=embed" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" width="400" height="240"></iframe>
				<br>
				<small>Ver <a href="https://maps.google.com.mx/maps/ms?msa=0&amp;msid=201896345870942678455.0004d484579947894e95b&amp;ie=UTF8&amp;t=m&amp;ll=21.108964,-101.670506&amp;spn=0.004804,0.008562&amp;z=16&amp;source=embed">Promehbsa, S.A. de C.V.</a> en un mapa ampliado</small>
			</div>
		</div> -->

	</div>
</div>
