<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns description-product">

		<h1 class="h1-text">Prevención de Infecciones</h1>

		<div class="row expanded">
			<div class="small-5 columns">
				<img class="thumbnail" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/product/<?php echo $presentaciones[0]->image; ?>" data-open="gallery">
			</div>
			<?php if( isset($presentaciones[0]) ): ?>
				<div class="small-7 columns">
					<p>Tipo de producto: <strong><?php echo $presentaciones[0]->nombre; ?></strong></p>
					<p>
						<?php echo $presentaciones[0]->descripcion_gral; ?>
					</p>
				</div>
			<?php endif; ?>
		</div>

		<div class="table-price">
			<div class="tr-table row column">
				<div class="small-6 columns">Producto</div>
				<div class="text-center small-3 columns">Precio</div>
				<div class="small-3 columns"></div>
			</div>

			<?php foreach ($presentaciones as $presentacion): ?>
				<div class="tr-table row column">
					<div class="small-6 columns"><b><?php echo $presentacion->codigo; ?></b> - <?php echo $presentacion->descripcion_pres; ?></div>
					<div class="text-center small-3 columns"><span class="price">$ <?php echo $presentacion->precio; ?></span></div>
					<div class="small-3 columns">
						<div class="select-product">
							<button type="button" name="button" class="small-3 columns select-number">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</button>
							<input class="small-6 columns input_count" type="text" name="" value="0" placeholder="0" data-id="<?php echo $presentacion->id; ?>">
							<button type="button" name="button" class="small-3 columns select-number">
								<i class="fa fa-plus" aria-hidden="true"></i>
							</button>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

			<div class="tr-table row column">
				<button type="button" name="button" class="button float-right add_cart_mlt">Agregar al carrito</button>
			</div>

		</div>


	</div>
</div>
