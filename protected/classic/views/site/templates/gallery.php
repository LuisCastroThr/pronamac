<div class="orbit gallery-home" role="region" aria-label="Favorite Space Pictures" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
    <ul class="orbit-container" tabindex="0">
        <button class="orbit-previous" aria-label="previous" tabindex="0"><span class="show-for-sr">Previous Slide</span>◀</button>
        <button class="orbit-next" aria-label="next" tabindex="0"><span class="show-for-sr">Next Slide</span>▶</button>
        <li class="orbit-slide" data-slide="0" style="max-height: ; display: none;">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/new/chloraprep.jpg">
        </li>
        <li class="orbit-slide" data-slide="1" style="position: relative; max-height: ; top: 0px; display: none;">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/new/agujas.jpg">

        </li>
        <li class="orbit-slide is-active" data-slide="2" style="position: relative; max-height: ; top: 0px; display: block;" aria-live="polite">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/new/insyte.jpg">
        </li>
		<li class="orbit-slide" data-slide="0" style="max-height: ; display: none;">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/new/kitpronamac.jpg">
        </li>
		<li class="orbit-slide" data-slide="0" style="max-height: ; display: none;">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/new/oxivir.jpg">
        </li>
		<li class="orbit-slide" data-slide="0" style="max-height: ; display: none;">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/new/posiflush.jpg">
        </li>
    </ul>
</div>
