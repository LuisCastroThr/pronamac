<div class="categories cart">
    <p class="title-categories">Resumen de Compra</p>
	<div class="cart carrito_div_detail">
		<ul>
			<li>Subtotal</li>
			<li class="price car_subtotal"><?php echo $detalle['subTotal']; ?></li>
		</ul>
		<ul>
			<li>Envío</li>
			<li class="car_envio"><?php echo $detalle['envio']; ?></li>
		</ul>
		<ul>
			<li>IVA (16%)</li>
			<li class="car_iva"><?php echo $detalle['iva']; ?></li>
		</ul>
		<ul>
			<li>Total</li>
			<li class="price car_total"><?php echo $detalle['total']; ?></li>
		</ul>
		<p class="text-center message-small"><small>No más de 10 Kg, sin cristalería y si soluciones</small></p>
	</div>

	<a href="<?php echo Yii::app()->createAbsoluteUrl('pncCarrito/CartStepTwo'); ?>" class="button expanded">Realizar compra</a>
</div>
