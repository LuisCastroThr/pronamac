<?php if(!empty($data->image)): ?>
	<div class="small-4 large-4 columns m-b box-product">
		<div class="product">
			<div class="img-product">
				<img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/product/<?php echo $data->image; ?>" alt="">
			</div>
			<div class="description-product">
				<h2 class="h2-product truncate"><?php echo $data->nombre; ?></h2>
				<?php if(!Yii::app()->user->isGuest): ?>
					<p class="price">$ <?php echo number_format($data->precio,2); ?></p>
				<?php endif; ?>
				<a href="<?php echo Yii::app()->createUrl('site/view_product',array('id'=>$data->id)); ?>" class="button expanded">Ver producto</a>
			</div>
		</div>
	</div>
<?php else: ?>
	<div class="tr-table-list row column">
		<div class="small-6 columns"><?php echo $data->nombre; ?></div>
		<div class="text-center small-3 columns">
			<?php if(!Yii::app()->user->isGuest): ?>
				<span class="price">$ <?php echo number_format($data->precio,2); ?></span>
			<?php endif; ?>
		</div>
		<div class="small-3 columns">
			<div class="select-product">
				<button type="button" name="button" class="small-3 columns select-number">
					<i class="fa fa-minus" aria-hidden="true"></i>
				</button>
				<input class="small-6 columns input_count" type="text" name="" value="0" placeholder="0" data-id="<?php echo $data->id; ?>">
				<button type="button" name="button" class="small-3 columns select-number">
					<i class="fa fa-plus" aria-hidden="true"></i>
				</button>
			</div>
		</div>
	</div>
<?php endif; ?>
