<footer>
    <div class="row column">
        <div class="small-12 medium-4 large-4 columns">
            <h4 class="text-center">Contáctanos</h4>
            <?php
                $model=new ContactForm;
                $form=$this->beginWidget('CActiveForm',
                    array(
                        'id'=>'contact-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )
                );
            ?>
                <div class="row">
                    <?php echo $form->textField($model,'name', array('placeholder' => 'Nombre')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textField($model,'phone', array('placeholder' => 'Teléfono')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textField($model,'email', array('placeholder' => 'Correo')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textField($model,'city', array('placeholder' => 'Ciudad o País')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50, 'placeholder' => 'Comentario')); ?>
                </div>

                <?php if(CCaptcha::checkRequirements()): ?>
                    <div class="row">
                        <div>
                            <?php $this->widget('CCaptcha'); ?>
                            <?php
                                echo $form->textField($model,'verifyCode',
                                    array(
                                        'placeholder'   => 'Código',
                                        'class'         => 'small-7 large-7 columns float-right',
                                    )
                                );
                            ?>
                            <?php echo $form->error($model,'verifyCode'); ?>
                         </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <?php
                        echo CHtml::submitButton('Contact',
                            array(
                                'class' => 'button expanded',
                                'id'    => 'btn_contact',
                                'value' => 'Contactar'
                            )
                        );
                    ?>
                </div>
            <?php $this->endWidget(); ?>
        </div>


        </div>
        <div class="small-12 medium-4 large-4 columns">
            <h4 class="text-center">Más información</h4>
            <p>Matriz Distrito Federal</p>
            <p>Pronamac S.A. de C.V. <br>
                5 de Febrero N° 809, Col. Álamos, Delegación Benito Juárez Mexico, Distrito Federal 03400
            </p>
            <p>Tel: (55)5590 - 1321</p>
            <p>Email: ventas_pronamac@prodigy.net.mx</p>
        </div>
    </div>
</footer>
