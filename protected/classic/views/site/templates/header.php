<button type="button" class="button" data-toggle="offCanvas" style="display: none;">Open Menu</button>

<div class="navigation">
    <div class="nav-costumer column">
        <div class="top-bar-left row">
            <ul class="menu row">
                <li class="social-media"><a href="https://www.facebook.com/Pronamac/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li class="social-media"><a href="https://twitter.com/Pronamac" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li ><a href="mailto:ventas_pronamac@prodigy.net.mx"><i class="fa fa-envelope" aria-hidden="true"></i>ventas_pronamac@prodigy.net.mx</a></li>
                <li ><span><i class="fa fa-phone" aria-hidden="true"></i></i>5590 - 1321</span></li>
            </ul>
        </div>

        <div class="top-bar-right">
            <ul class="menu">
                <?php if(Yii::app()->user->isGuest): ?>
                    <li><a data-open="modalRegister"><i class="fa fa-user" aria-hidden="true"></i> <span>Registrarse</span></a></li>
                    <li><a data-open="modalLogin"><i class="fa fa-sign-in" aria-hidden="true" data-open="modalLogin"></i> <span>Iniciar sesión</span> </a></li>
                    <li id="icon_cart_add"><a data-open="modalLogin"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>Carrito</span><span class="number">[0]</span></a></li>
                <?php else: ?>
                    <li><a href="<?php echo Yii::app()->createUrl('pncUsers/perfil/'); ?>"><i class="fa fa-user" aria-hidden="true"></i> <span>Perfil</span></a></li>
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('site/logout'); ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> <span>Cerrar sesión</span> </a></li>
                    <li id="icon_cart_add"><a class="cart-button" href="<?php echo Yii::app()->createAbsoluteUrl('pncCarrito/ViewCart'); ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>Carrito</span><span class="number">[<?php echo Tools::getTotalCart(); ?>]</span></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <div class="nav-site row column">
        <div class="small-3 large-3 nav-site-logo">
            <a href="/"><img class="branding" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/brand/logo-pronamac.jpg" width="297" alt="Pronamac"></a>
        </div>

        <div class="nav-menu">
            <ul class="dropdown menu main-menu" data-dropdown-menu>
                <li><a href="quienes-somos.php">Quiénes Somos</a></li>
                <li><a href="productos.php">Productos</a>
                    <ul class="menu">
                        <?php
                            $criteria = new CDbCriteria();
                            $criteria->select = 't.id, t.name';
                            $proveedor = PncProveedores::model()->findAll($criteria);
                            foreach ($proveedor as $categoria):
                        ?>
                            <li>
                                <a href="#">
                                    <span><?php echo $categoria->name; ?></span>
                                </a>
                                <ul class="menu">
                                    <?php
                                        $criteria = new CDbCriteria();
                                        $criteria->select = 't.id_categoria';
                                        $criteria->distinct = true;
                                        $criteria->addCondition('t.id_proveedor = '.$categoria->id);
                                        $prov = PncProductos::model()->findAll($criteria);
                                        foreach ($prov as $prov):
                                    ?>
                                        <li>
                                            <?php if(isset($prov->categoria)): ?>
                                            <a href="<?php echo Yii::app()->createUrl('site/Products', array('proveedor_id' => $prov->categoria->id)); ?>">
                                                <?php echo $prov->categoria->name; ?>
                                            </a>
                                        <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li><a href="articulos.php">Artículos</a></li>
                <li><a href="contacto.php">Contacto</a></li>
            </ul>
        </div>

		<div class="nav-search">
			<form action="<?php echo Yii::app()->createUrl('site/Products'); ?>" method="post" id="frm_search">
				<input class="top-bar-left" type="text" name="" placeholder="Buscar porducto">
				<i class="fa fa-search" aria-hidden="true"></i>
			</form>
		</div>
    </div>
</div>


<?php include 'modal.php'; ?>
