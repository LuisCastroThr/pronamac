<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">

		<h1 class="h1-text"><?php echo $titleName; ?></h1>

		<div class="row expanded m-b">
			<?php
			    $this->widget('zii.widgets.CListView', array(
			        'id' 				=>'ListaProductos',
			        'emptyText'			=>"No se encontraron resultados",
			        'afterAjaxUpdate' 	=>'js:function(){

			        }',
			        'dataProvider'		=> $data,
			        'summaryText'		=> '',
			        'itemView'			=> 'templates/listProducts',
			        'htmlOptions'		=> array(
			        ),
			        'pager'				=> array(
			            'header'		=> false,
			            'firstPageLabel'=> '&lt;&lt;',
			            'prevPageLabel'	=> '&lt;',
			            'nextPageLabel'	=> '&gt;',
			            'lastPageLabel'	=> '&gt;&gt;',
			        )
			    ));
			?>
		</div>
		<div class="row expanded">
			<div class="tr-table column">
				<button type="button" name="button" class="button float-right add_cart_mlt">Agregar al carrito</button>
			</div>
		</div>
	</div>
</div>
