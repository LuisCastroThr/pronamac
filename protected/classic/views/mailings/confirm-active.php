<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	</head>

	<body>
	    <table width="100%" style="background: #f5f5f5;" align="center" cellpadding="0" cellspacing="0">
		    <tr>
		      	<td style="padding: 20px 0;">
			        <table align="center" width="600" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="color: #666;font-family: Helvetica, Verdana, Arial; font-size:12px; text-align:justify;margin: 0 auto;">
						<tr>
							<td>
								<?php $this->renderPartial('/mailings/templates/header'); ?>
							</td>
						</tr>

						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" style="width: 100%; font-family: Helvetica, Verdana, Arial; color: #666666; font-size: 14pt; padding:0 40px 20px 40px;">
									<tr>
										<td>
											<table style="border-top: 1px solid #e2e2e2; width: 100%; padding: 20px 0;">
												<tr>
													<td>
														<p class="title">Hola Nombre,</p>
												        <p>¡Excelente! Tu cuentaha sido activada.</p>
													</td>
												</tr>
												<tr>
													<td style="text-align: center;">
														<a class="btn_active" style="clear: both; text-transform: uppercase;background: #23AA58; color: #ffffff ;cursor: pointer;text-decoration: none;padding: 10px 25px;display: inline-block;" href="">
                                                            Ver productos
                                                        </a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" style="width: 100%; font-family: Helvetica, Verdana, Arial; color: #999999; font-size: 11pt; padding:0 40px;">
									<tr>
										<td>
											<table style="border-top: 1px solid #e2e2e2; width: 100%;">
												<tr>
													<td>
														<p>Hasta pronto, <br> Equipo de Pronamac</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<?php $this->renderPartial('/mailings/templates/footer'); ?>
				</td>
			</tr>
		</table>
	</body>
</html>
