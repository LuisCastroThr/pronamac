<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	</head>

	<body>
	    <table width="100%" style="background: #f5f5f5;" align="center" cellpadding="0" cellspacing="0">
		    <tr>
		      	<td style="padding: 20px 0;">
			        <table align="center" width="600" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="color: #666;font-family: Helvetica, Verdana, Arial; font-size:12px; text-align:justify;margin: 0 auto;">
						<tr>
							<td>
								<?php $this->renderPartial('/mailings/templates/header'); ?>
							</td>
						</tr>

						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" style="width: 100%; font-family: Helvetica, Verdana, Arial; color: #666666; font-size: 14pt; padding:0 40px 20px 40px;">
									<tr>
										<td>
											<table style="border-top: 1px solid #e2e2e2; width: 100%; padding: 20px 0;">
												<tr>
													<td>
														<p class="title">Hola,</p>
												        <p>Se realizó una compra exitosamente.</p>
													</td>
												</tr>
												<tr>
													<td style="text-align: center; text-transform: uppercase; padding: 15px 0 10px;">
														<strong>Detalles de compra</strong>
													</td>
												</tr>
												<tr>
													<td>Nombre de Usuario: <span style="color: #999;">Persona que realizo compra</span></td>
												</tr>
												<tr>
													<td>Número de pedido: <span style="color: #23AA58;">12345</span> </td>
												</tr>
												<tr>
													<td>Fecha de Compra: <span style="color: #999999;">14/67/1988</span> </td>
												</tr>
												<tr>
													<td>Dirección de entrega: <span style="color: #999;">Niños Heroés de Chapultepec 151. Int. 404. Col. Niños Héroes de Chapultepec.</span> </td>
												</tr>

												<tr>
													<td>
														<table cellpadding="0" cellspacing="0" style="font-size: 11pt; width: 100%; padding:20px 0;">
															<thead style="">
																<tr>
																	<th style="padding: 15px 0; border-bottom: 2px solid #e2e2e2;"><strong>Producto</strong></th>
																	<th style="text-align: center; padding: 15px 0; border-bottom: 2px solid #e2e2e2;"><strong>Cantidad</strong></th>
																	<th style="text-align: center; padding: 15px 0; border-bottom: 2px solid #e2e2e2;"><strong>Precio Unitario</strong></th>
																	<th style="text-align: center; padding: 15px 0; border-bottom: 2px solid #e2e2e2;"><strong>Total</strong></th>
																</tr>
															</thead>
															<tbody style="color: #9999;">
																<tr>
																	<td style="padding: 10px 0; border-bottom: 1px solid #e2e2e2;">Nombre de Producto</td>
																	<td style="text-align: center; padding: 10px 0; border-bottom: 1px solid #e2e2e2;">2</td>
																	<td style="text-align: center; padding: 10px 0; border-bottom: 1px solid #e2e2e2;">$500</td>
																	<td style="text-align: center; padding: 10px 0; border-bottom: 1px solid #e2e2e2;">$1000</td>
																</tr>
																<tr>
																	<td style="padding: 10px 0; border-bottom: 1px solid #e2e2e2;">Nombre de Producto</td>
																	<td style="text-align: center; padding: 10px 0; border-bottom: 1px solid #e2e2e2;">2</td>
																	<td style="text-align: center; padding: 10px 0; border-bottom: 1px solid #e2e2e2;">$500</td>
																	<td style="text-align: center; padding: 10px 0; border-bottom: 1px solid #e2e2e2;">$1000</td>
																</tr>
															</tbody>
															<tfoot>
																<tr>
																	<td style="padding: 5px 0 5px;"></td>
																	<td style="text-align: center; padding: 5px 0 5px;"></td>
																	<td style="text-align: center; padding: 5px 0 5px; border-bottom: 1px solid #e2e2e2;">Envío</td>
																	<td style="text-align: center; padding: 5px 0 5px; border-bottom: 1px solid #e2e2e2;">$1000</td>
																</tr>
																<tr>
																	<td style="padding: 5px 0 5px;"></td>
																	<td style="text-align: center; padding: 5px 0;"></td>
																	<td style="text-align: center; padding: 5px 0; border-bottom: 1px solid #e2e2e2;">Subtotal</td>
																	<td style="text-align: center; padding: 5px 0; border-bottom: 1px solid #e2e2e2;">$1000</td>
																</tr>
																<tr>
																	<td style="padding: 5px 0 5px;"></td>
																	<td style="text-align: center; padding: 5px 0;"></td>
																	<td style="text-align: center; padding: 5px 0; border-bottom: 1px solid #e2e2e2;">Iva</td>
																	<td style="text-align: center; padding: 5px 0; border-bottom: 1px solid #e2e2e2;">$1000</td>
																</tr>
																<tr>
																	<td style="padding: 5px 0 5px;"></td>
																	<td style="text-align: center; padding: 5px 0;"></td>
																	<td style="text-align: center; padding: 5px 0; border-bottom: 1px solid #e2e2e2;">Total</td>
																	<td style="text-align: center; padding: 5px 0; border-bottom: 1px solid #e2e2e2; color:#23AA58;">$1000</td>
																</tr>
															</tfoot>
														</table>
													</td>
												</tr>

												<tr>
													<td>
														<table cellpadding="0" cellspacing="0" style="width: 100%; font-family: Helvetica, Verdana, Arial; color: #666666; font-size: 14pt; padding: 0 0 20px 0;">
															<tr>
																<td style="text-align: center; text-transform: uppercase; padding: 15px 0 10px;">
																	<strong>Detalles de Facturación</strong>
																</td>
															</tr>
															<tr>
																<td>Nombre de la razón social: <span style="color: #999999;">Razón Social</span></td>
															</tr>
															<tr>
																<td>RFC: <span style="color: #23AA58;">12345</span> </td>
															</tr>
															<tr>
																<td>Dirección: <span style="color: #999999;">Niños Heroés de Chapultepec</span> </td>
															</tr>
															<tr>
																<td>Estado: <span style="color: #999999;">Niños Heroés de Chapultepec</span> </td>
															</tr>
															<tr>
																<td>Ciudad: <span style="color: #999999;">Niños Heroés de Chapultepec</span> </td>
															</tr>
															<tr>
																<td>Colonia: <span style="color: #999999;">Niños Heroés de Chapultepec</span> </td>
															</tr>
															<tr>
																<td>Calle y Número: <span style="color: #999999;">Niños Heroés de Chapultepec</span> </td>
															</tr>
															<tr>
																<td>Código postal: <span style="color: #999999;">Niños Heroés de Chapultepec</span> </td>
															</tr>
															<tr>
																<td>Teléfono: <span style="color: #999999;">56-66-66-66</span> </td>
															</tr>
															<tr>
																<td>Correo electrónico: <a style="color: #999999; text-decoration: none;">mhroman17@gmail.com</a> </td>
															</tr>
														</table>
													</td>
												</tr>


												<tr>
													<td style="text-align: center;">
														<a class="btn_active" style="clear: both; text-transform: uppercase;background: #23AA58; color: #ffffff ;cursor: pointer;text-decoration: none;padding: 10px 25px;display: inline-block;" href="">
                                                            Descargar archivo de compra
                                                        </a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" style="width: 100%; font-family: Helvetica, Verdana, Arial; color: #999999; font-size: 11pt; padding:0 40px;">
									<tr>
										<td>
											<table style="border-top: 1px solid #e2e2e2; width: 100%;">
												<tr>
													<td>
														<p>Hasta pronto, <br> Equipo de Pronamac</p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<?php $this->renderPartial('/mailings/templates/footer'); ?>
				</td>
			</tr>
		</table>
	</body>
</html>
