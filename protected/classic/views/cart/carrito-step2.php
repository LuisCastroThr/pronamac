<div class="wrapper-general row">
	<h1 class="small-12 large-12 columns h1-text">Datos de Compra</h1>
	<div class="small-9 large-9 columns">
		<div class="section-step">
			<p class="title-step">
				<span class="badge">1</span><span class="h2-text">Opciones de Envío</span>
			</p>

			<div class="select-option row">
					<div class="medium-4 columns">
						<input type="radio" id="send-2" checked>
						<label for="send-2" class="option-send select-send">
							Envíar a dirección
						</label>
					</div>
					<div class="medium-4 columns">
						<input type="radio" id="send-1">
						<label for="send-1" class="option-send select-pick-up">
							Recoger en sucursal
						</label>
					</div>
					<div class="medium-4 columns">
					</div>
				</div>

			<div class="subsection-step address-send" style="display:;">
				<p class="h2-text">Dirección de envío <small>Tiempo de entrega: 1 a 3 días hábiles</small></p>
				<div class="select-addres">
					<input class="float-right" type="checkbox" name="" value="" id="select-addres" checked>
					<label for="select-addres">
						<p>Dirección 1</p>
						<p>Niños Heroes de Chapultepec, Col. Niños Héroes de Chapultepec, C.P. 03300, Del. Benito Juarez, Ciudad México</p>
					</label>
				</div>
				<button class="button pull-right new-address success" type="button" name="button">Agregar Nueva Dirección</button>
			</div>

			<?php $this->renderPartial('/cart/location'); ?>

			<div class="subsection-step select-local" style="display: none;">
				<p class="h2-text">Selecciona Sucursal</p>
				<div class="select-addres">
					<input class="float-right" type="checkbox" name="" value="" id="select-mexico">
					<label for="select-mexico">
						<p class="subtitle"><strong>Matriz Ciudad de México</strong> | Pronamac, S.A. de C.V.</p>
						<p class="truncate">5 de Febrero N° 809, Col. Álamos, Delegación Benito Juárez, C.P. 03400 Ciudad de México</p>
					</label>
				</div>

				<div class="select-addres">
					<input class="float-right" type="checkbox" name="" value="" id="select-jalisco">
					<label for="select-jalisco">
						<p class="subtitle"><strong>Sucursal Zapopan, Jalisco</strong> | Pronamac de Occidente</p>
						<p class="truncate">Giosue Carducci No.5541 Col. Jardines Vallarta, C.P. 45150 Zapopan, Jalisco</p>
					</label>
				</div>

				<div class="select-addres">
					<input class="float-right" type="checkbox" name="" value="" id="select-mty">
					<label for="select-mty">
						<p class="subtitle"><strong>Sucursal Monterrey </strong></p>
						<p class="truncate">Gregorio Salinas Varona No. 250 Col. Burócratas del Estado C.P. 64380 Monterrey  N.L</p>
					</label>
				</div>
			</div>
		</div>

		<div class="section-step">
			<p class="title-step">
				<span class="badge">2</span><span class="h2-text">Solicitar Facturar</span>
			</p>

			<div class="select-option row">
				<div class="medium-4 columns">
					<input type="radio" id="facturar">
					<label for="facturar" class="option-send check-in">
						Solicitar factura
					</label>
				</div>
				<div class="medium-4 columns">
					<input type="radio" id="no-facturar">
					<label for="no-facturar" class="option-send no-bill">
						No solicitar factura
					</label>
				</div>
				<div class="medium-4 columns">
				</div>
			</div>

			<div class="subsection-step show-check-in" style="display: none;">
				<p class="h2-text">Datos de Facturación</p>
				<div class="select-addres">
					<input type="checkbox" name="" value="" id="factuacion">
					<label for="factuacion">
						<div class="medium-6 columns">
							<p><strong>Nombre ó Razón social:</strong> Mariana Herrera Roman</p>
							<p><strong>RFC:</strong> Pronamac</p>
							<p><strong>Dirección Fiscal:</strong> Niños Heroes de Chapultepec, Col. Niños Héroes de Chapultepec, C.P. 03300, Del. Benito Juarez, Ciudad México</p>
						</div>
						<div class="medium-6 columns">
							<p><strong>Tipo de Facturación:</strong> Pronamac</p>
							<p><strong>Teléfono:</strong> Pronamac</p>
							<p><strong>Correo:</strong> Pronamac</p>
						</div>
					</label>
				</div>
				<button class="button pull-right new-check-in" type="button" name="button">Agregar Nuevos Datos</button>
			</div>

			<div class="subsection-step generate-check-in" style="display: none;">
				<p class="h2-text">Ingresa datos de facturación</p>
				<form class="form-general">
					<div class="row">
						<div class="medium-6 columns">
							<label>Nombre o razón social
								<input type="text" placeholder="Nombre o razón social">
							</label>
						</div>
						<div class="medium-6 columns">
							<label>RFC
								<input type="text" placeholder="RFC">
							</label>
						</div>
					</div>

					<div class="row">
						<div class="medium-6 columns">
							<label>Tipo de facturación
								<select class="" name="">
									<option value="">Persona moral</option>
									<option value="">Persona física</option>
								</select>
							</label>
						</div>
						<div class="medium-6 columns">
							<label>Calle
								<input type="text" placeholder="Calle">
							</label>
						</div>
					</div>


					<div class="row">
						<div class="medium-3 columns">
							<label>No. Exterior
								<input type="text" placeholder="No. Exterior">
							</label>
						</div>
						<div class="medium-3 columns">
							<label>No. Interior
								<input type="text" placeholder="No. Interior">
							</label>
						</div>
						<div class="medium-6 columns">
							<label>Código Postal
								<input type="text" placeholder="Código Postal">
							</label>
						</div>
					</div>

					<div class="row">
						<div class="medium-6 columns">
							<label>Estado
								<select class="" name="">
									<option value="">Seleccion un Estado</option>
								</select>
							</label>
						</div>
						<div class="medium-6 columns">
							<label>Ciudad
								<select class="" name="">
									<option value="">Seleccion un Estado</option>
								</select>
							</label>
						</div>
					</div>

					<div class="row">
						<div class="small-6 medium-6 columns">
							<label>Delegación
								<select class="" name="">
									<option value="">Seleccion un Estado</option>
								</select>
							</label>
						</div>
						<div class="medium-6 columns">
							<label>Colonia
								<select class="" name="">
									<option value="">Seleccion un Estado</option>
								</select>
							</label>
						</div>
					</div>

					<div class="row">
						<div class="small-6 medium-6 columns">
							<label>Teléfono
								<input type="text" placeholder="Teléfono">
							</label>
						</div>
						<div class="medium-6 columns">
							<label>Correo
								<input type="text" placeholder="Correo">
							</label>
						</div>
					</div>

					<div class="row">
						<div class="medium-6 columns"></div>
						<div class="medium-6 columns">
							<input class="button expanded m-t-40" type="submit" value="Guardar datos">
						</div>
					</div>
				</form>
			</div>
		</div>


	</div>

	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/aside-cart', array('detalle'=>$detalle)); ?>
	</div>
</div>
