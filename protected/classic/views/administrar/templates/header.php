<div class="navigation">
    <div class="nav-costumer column">
        <div class="top-bar-left row">
            <ul class="menu row">
                <li class="social-media"><a><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li class="social-media"><a><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li class="button-contact-menu"><a href="mailto:ventas_pronamac@prodigy.net.mx"><i class="fa fa-envelope" aria-hidden="true"></i>ventas_pronamac@prodigy.net.mx</a></li>
                <li class="button-contact-menu"><i class="fa fa-phone" aria-hidden="true"></i></i>5590 - 1321</li>
			</ul>
        </div>

		<div class="top-bar-right">
            <ul class="menu">
                <?php if(Yii::app()->user->isGuest): ?>
                    <li><a data-open="modalLogin"><i class="fa fa-sign-in" aria-hidden="true" data-open="modalLogin"></i> Iniciar sesión </a></li>
                <?php else: ?>
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('site/logout'); ?>"><i class="fa fa-sign-in" aria-hidden="true" data-open="modalLogin"></i> Cerrar sesión </a></li>
                <?php endif; ?>
			</ul>
        </div>
    </div>

	<div class="nav-site row column">
        <div class="small-3 large-3 nav-site-logo">
            <a href="/"><img class="branding" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/brand/logo-pronamac.jpg" width="297" alt="Pronamac"></a>
        </div>
    </div>
</div>
