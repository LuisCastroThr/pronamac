var xhr;

$(document).on('click', '.select-send', function(e){
	$('.select-local').hide();
});

$(document).on('click', '.new-address', function(e){
	$('.generate-address').show();
	$('.select-local').hide();
	$('.generate-address').addClass('fadeIn animated');
});

$(document).on('click', '.select-pick-up', function(e){
	$('.address-send').hide();
	$('.select-local').show();
	$('.select-local').addClass('fadeIn animated');
});

$(document).on('click', '.check-in', function(e){
	$('.show-check-in').show();
	$('.show-check-in').addClass('fadeIn animated');
});

$(document).on('click', '.new-check-in', function(e){
	$('.generate-check-in').show();
	$('.generate-check-in').addClass('fadeIn animated');
});

$(document).on('click', '.no-bill', function(e){
	$('.show-check-in').hide();
	$('.generate-check-in').hide();
});

$(document).on('click','.erase-article',function(){
	var id_item = $(this).attr('data-id');

	$.ajax({
        url: Pronamac.Object.urlTest+'/pncCarrito/DeleteItem',
        dataType: 'json',
        type: 'POST',
        data: {
            id_item: id_item
        },
        beforeSend: function () {
            
        },
        complete: function () {
            
        },
        success: function (data) {
    		if(data.success){
    			Pronamac.Object.modal.dialog().html(data.messages).dialog({
    				close: function(){
    					location.reload();
    				}
    			}).dialog('open');
    		}
        }
    });
});

function UpdateCart(id_item, cantidad)
{
	if(xhr && xhr.readyState != 4) { xhr.abort(); }
                
	xhr = $.ajax({
        url: Pronamac.Object.urlTest+'/pncCarrito/UpdateCart',
        dataType: 'json',
        type: 'POST',
        data: {
            item: id_item,
            cantidad: cantidad
        },
        beforeSend: function () {
            
        },
        complete: function () {
            
        },
        success: function (data) {
    		if(data.success){
    			//Item
    			$('.div_'+data.item.id+' .text-center p').html(data.item.monto);

    			//Detalle
    			$('.carrito_div_detail .car_subtotal').html(data.detalle.subTotal);
    			$('.carrito_div_detail .car_envio').html(data.detalle.envio);
    			$('.carrito_div_detail .car_iva').html(data.detalle.iva);
    			$('.carrito_div_detail .car_total').html(data.detalle.total);
    		}
        }
    });
}