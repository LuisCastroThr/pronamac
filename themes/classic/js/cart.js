var xhr;

$(document).on('click', '.select-send', function(e){
    $('#send-1').attr('checked',false);
    $('#send-2').attr('checked',true);

    //Quita check de radios y active
    $("input[name='sucursal']:checked").attr('checked',false);
    $('.select-addres').removeClass('active');
    
	$('.select-local').hide();
    $('.address-send').show();
    $('.address-send').addClass('fadeIn animated');
    $(this).addClass('active');
    $('.select-pick-up').removeClass('active');

});

$(document).on('click', '.new-address', function(e){
	$('.generate-address').show();
	$('.select-local').hide();
	$('.generate-address').addClass('fadeIn animated');
});

$(document).on('click', '.select-pick-up', function(e){
    $('#send-2').attr('checked',false);
    $('#send-1').attr('checked',true);

    //Quita check de radios y active
    $("input[name='sucursal']:checked").attr('checked',false);
    $('.select-addres').removeClass('active');

    
	$('.address-send').hide();
	$('.select-local').show();
	$('.select-local').addClass('fadeIn animated');
    $(this).addClass('active');
    $('.select-send').removeClass('active');
});

$(document).on('click', '.check-in', function(e){
    $('#no-facturar').attr('checked',false);
	$('.show-check-in').show();
	$('.show-check-in').addClass('fadeIn animated');
    $(this).addClass('active');
    $('.no-bill').removeClass('active');
});

$(document).on('click', '.new-check-in', function(e){
    $('.generate-check-in').show();
    $('.generate-check-in').addClass('fadeIn animated');
});


$(document).on('click', '.select-addres', function(e){
	$(this).addClass("active").siblings().removeClass("active");
});



$(document).on('click', '.no-bill', function(e){
    $('#facturar').attr('checked',false);
	$('.show-check-in').hide();
	$('.generate-check-in').hide();
    $(this).addClass('active');
    $('.check-in').removeClass('active');
});

$(document).on('click','.erase-article',function(){
	var id_item = $(this).attr('data-id');

	$.ajax({
        url: Pronamac.Object.urlTest+'/pncCarrito/DeleteItem',
        dataType: 'json',
        type: 'POST',
        data: {
            id_item: id_item
        },
        beforeSend: function () {
            
        },
        complete: function () {
            
        },
        success: function (data) {
    		if(data.success){
    			Pronamac.Object.modal.dialog().html(data.messages).dialog({
    				close: function(){
    					location.reload();
    				}
    			}).dialog('open');
    		}
        }
    });
});

function UpdateCart(id_item, cantidad)
{
	if(xhr && xhr.readyState != 4) { xhr.abort(); }
                
	xhr = $.ajax({
        url: Pronamac.Object.urlTest+'/pncCarrito/UpdateCart',
        dataType: 'json',
        type: 'POST',
        data: {
            item: id_item,
            cantidad: cantidad
        },
        beforeSend: function () {
            
        },
        complete: function () {
            
        },
        success: function (data) {
    		if(data.success){
    			//Item
    			$('.div_'+data.item.id+' .text-center p').html(data.item.monto);

    			//Detalle
    			$('.carrito_div_detail .car_subtotal').html(data.detalle.subTotal);
    			$('.carrito_div_detail .car_envio').html(data.detalle.envio);
    			$('.carrito_div_detail .car_iva').html(data.detalle.iva);
    			$('.carrito_div_detail .car_total').html(data.detalle.total);
    		}
        }
    });
}


//VALIDA DATOS DE ENVIO
$(document).on('click','#check_send_cart',function(){
    var ts = $("input[name='type_send']:checked");
    var su = $("input[name='sucursal']:checked").length;

    var fa = $("input[name='facturas']:checked");
    var inv = $("input[name='invoices']:checked").length;

    //Tipo de envio
    if( !ts.length ){
        Pronamac.Object.modal.dialog().html('Seleccione una opción de envío.').dialog({}).dialog('open');
        return false;
    }

    //Sucursal
    if( ts.attr('id') == 'send-1' &&  !su){
        Pronamac.Object.modal.dialog().html('Seleccione una sucursal.').dialog({}).dialog('open');
        return false;
    }

    //Direccion
    if( ts.attr('id') == 'send-2' &&  !su){
        Pronamac.Object.modal.dialog().html('Seleccione una dirección.').dialog({}).dialog('open');
        return false;
    }

    //Tipo de factura
    if( !fa.length ){
        Pronamac.Object.modal.dialog().html('Seleccione si requiere factura.').dialog({}).dialog('open');
        return false;
    }

    //Datos de factura
    if( fa.attr('id') == 'facturar' &&  !inv){
        Pronamac.Object.modal.dialog().html('Seleccione datos para facturar.').dialog({}).dialog('open');
        return false;
    }


    if(xhr && xhr.readyState != 4) { xhr.abort(); }          
    xhr = $.ajax({
        url: Pronamac.Object.urlTest+'/pncCarrito/SendPay',
        dataType: 'json',
        type: 'POST',
        data: {
            location: $("input[name='sucursal']:checked").val(),
            invoice:  $("input[name='facturas']:checked").val()
        },
        success: function (data) {
            if(data.success){
                var url = $('#check_send_cart').attr('data-url');
                window.location = url+'?cart_id='+data.cart_id;
            }
        }
    });
});