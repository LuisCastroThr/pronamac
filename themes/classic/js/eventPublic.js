//MENU CATEGORIAS
$(document).on('click','.mn_cat li', function(){
	$('.mn_cat li').removeClass('active');
	$(this).addClass('active');

	$('.mn_cat li').each(function( index ) {
		if( !$( this ).hasClass('active') )
		{
			$(this).children('ul').fadeOut();
		}
	});
});

//MENU PROVEEDORES
$(document).on('click','.mn_pro li', function(){
	$('.mn_pro li').removeClass('active');
	$(this).addClass('active');

	$('.mn_pro li').each(function( index ) {
		if( !$( this ).hasClass('active') )
		{
			$(this).children('ul').fadeOut();
		}
	});
});

//MENU PERFIL
$(document).on('click','.div_menu_perfil span',function(){
	if(  $(this).hasClass('perfil') ){
		$('.div_perfil_envio').fadeOut();
		$('.generate-address').fadeOut();
		$('.div_perfil_datos').fadeIn();
	}
	else{
		$('.div_perfil_datos').fadeOut();
		$('.div_perfil_envio').fadeIn();
		$('.generate-address').fadeIn();
	}
});

//CONTRO DE ADD / LESS 
$(document).on('click','.select-product button',function(){
	var input = $(this).parent('.select-product').find('input');
	var cant  = parseInt( $(input).val() );

	if( $(this).children().hasClass('fa-plus') )
		cant = cant + 1;
	else if( cant > 0) 
		cant = cant - 1;

	$(input).val(cant);

	if( $('.update_item_cart').is(':visible') ){
		var id_item = $(input).attr('data-id');
		UpdateCart(id_item,cant);
	}
});


//AGREGAR VARIOS PRDUCTOS AL CARRITO
$(document).on('click', '.add_cart_mlt', function(){
	var car = {};
	$('.input_count').each(function( index ) {
		if( $( this ).val() !== '0' )
			car[$(this).attr('data-id')] = $( this ).val();
	});

	if( Object.keys(car).length > 0 )
	{
		$.ajax({
	        url: Pronamac.Object.urlTest+'/pncCarrito/AddToCart',
	        dataType: 'json',
	        type: 'POST',
	        data: {
	            productos: car
	        },
	        beforeSend: function () {
	            
	        },
	        complete: function () {
	            
	        },
	        success: function (data) {
	    		if(data.success){
	    			//Limpiamos contadores de productos
	    // 			$('.input_count').each(function( index ) {
					// 	$( this ).val(0); 
					// });

	  				$('#icon_cart_add .number').html('['+data.totalItem+']');

	  				Pronamac.Object.modal.dialog().html('Producto agregado correctamente.').dialog('open');
	    		}

	    		if(data.error){
	    			Pronamac.Object.modal.dialog().html(data.error).dialog('open');
	    		}
	        }
	    });
	}	
	else
		Pronamac.Object.modal.dialog().html('Seleccione un producto.').dialog('open');
});

//BUSCADOR
$(document).on('keyup','#frm_search',function(){
	var term = $(this).find('input').val();
	if( term.length > 0)
	{
		$(this).attr('action',Pronamac.Object.urlTest+'/productos.php?term='+term);
	}
});

//Editar PERFIL
$(document).on('click','#btn_updateUser',function(e){
	e.preventDefault();
    
     $.ajax({
        type: 'POST',
        dataType: 'json',
        url: Pronamac.Object.urlTest+'/pncUsers/UpdatePerfil',
        data:  $('#user-form').serialize(),
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (data) {
        	if(data.success){
        		Pronamac.Object.modal.dialog().html(data.message).dialog('open');
        	}

        	if(data.error){
        		$('#user-form input').removeClass('is-invalid-input');
        		$.each(data.errors, function(index, value){
        			$('#RegisterForm_'+index).addClass('is-invalid-input');
        		});
        	}
        },
    });
});

//Editar Location
$(document).on('click','#send_location',function(e){
	e.preventDefault();
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: Pronamac.Object.urlTest+'/PncLocation/UpdateLocation',
        data:  {
        	LocationForm: $('#location-form').serialize()
        },
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (data) {
        	if(data.success){
        		Pronamac.Object.modal.dialog().html(data.message).dialog('open');
        		window.location.reload();
        	}

        	if(data.error){
        		$('#location-form input').removeClass('is-invalid-input');
        		$.each(data.error, function(index, value){
        			$('#LocationForm_'+index).addClass('is-invalid-input');
        		});
        	}
        },
    });
});

//Editar Factura
$(document).on('click','#send_factura',function(e){
	e.preventDefault();
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: Pronamac.Object.urlTest+'/PncFacturas/UpdateFactura',
        data:  {
        	FacturasForm: $('#factura-form').serialize()
        },
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (data) {
        	if(data.success){
        		Pronamac.Object.modal.dialog().html(data.message).dialog('open');
        		window.location.reload();
        	}

        	if(data.error){
        		$('#factura-form input').removeClass('is-invalid-input');
        		$.each(data.error, function(index, value){
        			$('#FacturasForm_'+index).addClass('is-invalid-input');
        		});
        	}
        },
    });
});

//Guarda pagina visitada
$(document).on('click','#yw0 .page a',function(e){
	var page 	 = parseInt( $(this).html() );
	var url 	 = null;
	var original = Pronamac.Object.search.original;
	
	if(page > 1){
		if( original.indexOf("?") > 0 )
			url = original+'&pagina='+page;
		else
			url = original+'?pagina='+page;
	} else {
		url = original;
	}
	
	History.replaceState('', document.title, url);

	$("html, body").animate({ scrollTop: 0 }, "slow");
});