$(document).on('click', '#btn_login', function(e){
	e.preventDefault();
	
	 $.ajax({
        type: 'POST',
        dataType: 'json',
        url: Pronamac.Object.urlTest+'/site/login',
        data:  $('#login-form').serialize(),
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (data) {
        	if(data.sucess){
        		window.location = data.redirect;
        	}

        	if(data.error){
        		$('#login-form input').removeClass('is-invalid-input');
        		$.each(data.error, function(index, value){
        			$('#LoginForm_'+index).addClass('is-invalid-input');
        		});
        	}
        },
    });
});

$(document).on('click', '#btn_register', function(e){
    e.preventDefault();
    
     $.ajax({
        type: 'POST',
        dataType: 'json',
        url: Pronamac.Object.urlTest+'/site/register',
        data:  $('#pnc-users-form').serialize(),
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (data) {
            if(data.sucess){
                window.location = data.redirect;
            }

            if(data.error){
                $('#pnc-users-form input').removeClass('is-invalid-input');
                $.each(data.error, function(index, value){
                    $('#RegisterForm_'+index).addClass('is-invalid-input');
                });
            }
        },
    });
});

$(document).on('click', '#btn_contact', function(e){
    e.preventDefault();
    
     $.ajax({
        type: 'POST',
        dataType: 'json',
        url: Pronamac.Object.urlTest+'/site/contactar',
        data:  $('#contact-form').serialize(),
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (data) {
            if(data.sucess){
                $('#contact-form')[0].reset();
                Pronamac.Object.modal.dialog().html('Correo enviado correctamente.').dialog('open');
            }
            if(data.error){
                $('#contact-form input').removeClass('is-invalid-input');
                $.each(data.error, function(index, value){
                    $('#ContactForm_'+index).addClass('is-invalid-input');
                });
            }
        },
    });
});