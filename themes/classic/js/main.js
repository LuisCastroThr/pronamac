(function (window) {
	'use strict';
  	var Pronamac = window.Pronamac || {};
  	
  	// Establecer opciones por omision
	Pronamac.Object = {
   		search: {
      		text: null,
            page: 1,
            original: null
    	},
	    user: {
	    	isGuest: true,
	    },
    	modal: $('<div></div>').html('Pronamac.com').dialog({
      		autoOpen: false,
      		resizable: false,
      		draggable: false,
      		modal: true,
      		title: 'Aviso'
    	}),
      urlTest: null
  	};
  	window.Pronamac = Pronamac;
}(window));

function getSearchParams(k){
    var p={};
    location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
    return k?p[k]:p;
}