<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes">


		<title>Pronamac</title>

		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/fontawesome/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/foundation.css">
 		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css">

 		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	</head>
	<body oncontextmenu="return false" onselectstart="return false" ondragstart="">
		<div class="off-canvas- wrapper">
			<div class="off-canvas position-left" id="offCanvas" data-off-canvas>
			</div>

			<div class="off-canvas-content" data-off-canvas-content>

				<?php $this->renderPartial('/site/templates/header'); ?>

				<?php echo $content; ?>

			</div>
			<?php $this->renderPartial('/site/templates/footer'); ?>
		</div>

		<?php
			Yii::app()->clientScript->registerCoreScript('jquery');
        	Yii::app()->clientScript->registerCoreScript('jquery.ui');

        	$cs=Yii::app()->clientScript;
        	$cs->registerScript('pronamac',"
        		Pronamac.Object.urlTest = '".Yii::app()->params['url_test']."';
        	", CClientScript::POS_READY);
		?>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/what-input.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/foundation.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/app.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/main.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/eventPublic.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/login.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/cart.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/history.js"></script>
	    <script type="text/javascript">
			function imgError(image) {
			    image.onerror = "";
			    image.src = Pronamac.Object.urlTest+"/themes/classic/src/product/no-foto.jpg";
			    return true;
			}
		</script>

	    <?php
	    	$cs->registerScript('pagination',"
	    		var categoria = getSearchParams('categoria_id');
	    		var proveedor = getSearchParams('proveedor_id');
        		var page = getSearchParams('pagina');
				
				if( page !== undefined ){
				 	$.fn.yiiListView.update('ListaProductos', {
				        url: Pronamac.Object.urlTest+'/site/Products',
				        data: {
				        	categoria_id: categoria,
				        	proveedor_id: proveedor,
				            PncProductos_page: page
				        }
				    });
				}
        	", CClientScript::POS_READY);
	   	?>
	</body>
</html>
