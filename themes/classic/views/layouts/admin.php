<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes">


		<title>Pronamac</title>

		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/fontawesome/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/foundation.css">
 		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css">


	</head>
	<body>
		<div class="off-canvas- wrapper admin">
			<div class="off-canvas position-left" id="offCanvas" data-off-canvas>
			</div>

			<div class="off-canvas-content" data-off-canvas-content>

				<?php $this->renderPartial('/administrar/templates/header'); ?>

				<?php echo $content; ?>
			</div>
		</div>

		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/jquery.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/what-input.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/vendor/foundation.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/app.js"></script>
	    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/login.js"></script>
	</body>
</html>
