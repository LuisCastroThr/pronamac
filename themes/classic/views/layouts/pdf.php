<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes">


		<title>Pronamac</title>

		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/foundation.css">
 		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdf.css">

 		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	</head>
	<body>
		<div class="wrapper-pdf row">
			<div class="page">
				<div class="header row">
					<div class="small-6 large-6 columns brand">
					  <img src="src/brand/pronamac.jpg" alt="Pronamac">
					</div>
					<div class="small-6 large-6 columns text-right">
						<br>
						<p><strong>Nombre del comprador</strong></p>
						<p>Número de pedido: 32423</p>
					</div>
				</div>

				<div class="row">
					<div class="small-7 large-7 columns">
						<p><strong>Nombre ó Razón social:</strong> Mariana Herrera Roman</p>
						<p><strong>RFC:</strong> Pronamac</p>
						<p><strong>Dirección Fiscal:</strong> Niños Heroes de Chapultepec, Col. Niños Héroes de Chapultepec, C.P. 03300, Del. Benito Juarez, Ciudad México</p>
						<p><strong>Teléfono:</strong> 232442332</p>
						<p><strong>Correo:</strong> sdasdasd@sadasd.com</p>
					</div>
					<div class="small-5 large-5 columns float-right text-right">
						<p><strong>Dirección de envío: </strong>Niños Heroes de Chapultepec, Col. Niños Héroes de Chapultepec, C.P. 03300, Del. Benito Juarez, Ciudad México</p>
					</div>
				</div>

				<div class="">
					<table class="unstriped">
						<thead>
							<tr>
								<th>Código</th>
								<th class="text-center">Producto</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">Precio Unitario</th>
								<th class="text-center">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Content Goes Here</td>
								<td>This is longer content Donec id elit non mi porta gravida at eget metus.</td>
								<td>Content Goes Here</td>
								<td>Content Goes Here</td>
								<td>Content Goes Here</td>
							</tr>
							<tr>
								<td>Content Goes Here</td>
								<td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
								<td>Content Goes Here</td>
								<td>Content Goes Here</td>
								<td>Content Goes Here</td>
							</tr>
							<tr>
								<td>Content Goes Here</td>
								<td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
								<td>Content Goes Here</td>
								<td>Content Goes Here</td>
								<td>Content Goes Here</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td class="border">Envío</td>
								<td class="border">$23123123</td>
							</tr>
								<td></td>
								<td></td>
								<td></td>
								<td class="border">Subtotal</td>
								<td class="border">$23123123</td>
							</tr>
								<td></td>
								<td></td>
								<td></td>
								<td class="border">Iva</td>
								<td class="border">$23123123</td>
							</tr>
								<td></td>
								<td></td>
								<td></td>
								<td class="border">Total</td>
								<td class="border bold">$23123123</td>
							</tr>
						</tfoot>

					</table>
				</div>
			</div>
		</div>
	</body>
</html>
