<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php include 'templates/nav-profile.php'; ?>
	</div>
	<div class="small-9 large-9 columns div_perfil_datos">
		<h1 class="h1-text">Datos de perfil</h1>
		<div class="row form-general">
			<?php 
		       $model = new RegisterForm;
		        $form = $this->beginWidget('CActiveForm', array(
		            'id'                    => 'user-form',
		            'enableClientValidation'=> true,
		            'clientOptions'         => array(
		                'validateOnSubmit'  => true,
		            )
		        )); 
		    ?>
				<div class="small-6 large-6 columns">
					<label>
						Nombre
						<?php 
			                echo $form->textField($model,'name', array(
			                        'placeholder'   => 'Nombre',
			                        'value' 		=> $user->name,
			                    )
			                ); 
			            ?>
					</label>
					<label>
						Contraseña
						<?php 
			                echo $form->passwordField($model, 'password',
			                    array(
			                        'size'        => 60,
			                        'maxlength'   => 200,
			                        'placeholder' => 'Contraseña'
			                    )
			                ); 
			            ?>
					</label>
					<label>
						Correo
						<?php 
			                echo $form->textField($model,'email', array(
			                        'placeholder'   => 'Correo',
			                        'value' 		=> $user->email,
			                        'disabled' 		=> true,
			                    )
			                ); 
			            ?>
					</label>
				</div>
				<div class="small-6 large-6 columns">
					<label>
						Apellidos
						<?php 
			                echo $form->textField($model,'lastname', array(
			                        'placeholder'   => 'Apellidos',
			                        'value' 		=> $user->lastname,
			                    )
			                ); 
			            ?>
					</label>
					<label>
						Confirmar Contraseña
						<?php 
			                echo $form->passwordField($model, 'repassword',
			                    array(
			                        'size'        => 60,
			                        'maxlength'   => 200,
			                        'placeholder' => 'Confirmar contraseña'
			                    )
			                ); 
			            ?>
					</label>
					<?php 
		                echo CHtml::submitButton('Update', array(
		                        'class' => 'm-t-20 button expanded',
		                        'value' => 'Cambiar Datos',
		                        'id'    => 'btn_updateUser'
		                    )
		                ); 
		            ?>
				</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>

	<div class="small-9 large-9 columns div_perfil_envio" style="display: none;">
		<h1 class="h1-text">Datos de envío</h1>
		<?php $this->renderPartial('/perfil/location', array('location'=>$location)); ?>
	</div>
</div>