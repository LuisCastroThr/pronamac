<?php 
    $model = new FacturasForm;
    $form = $this->beginWidget('CActiveForm', array(
        'id'                    => 'factura-form',
        'enableClientValidation'=> true,
        'clientOptions'         => array(
            'validateOnSubmit'  => true,
            'class'             => 'form-general'
        )
    )); 
?>
	<div class="row">
		<div class="medium-6 columns">
			<label>Nombre o razón social
				<?php 
	                echo $form->textField($model,'razon', array(
	                        'placeholder'   => 'Nombre o razón social',
	                        'value' 		=> $factura ? $factura->razon : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
		<div class="medium-6 columns">
			<label>RFC
				<?php 
	                echo $form->textField($model,'rfc', array(
	                        'placeholder'   => 'RFC',
	                        'value' 		=> $factura ? $factura->rfc : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
	</div>

	<div class="row">
		<div class="medium-6 columns">
			<label>Tipo de facturación
				<?php 
	                echo $form->dropDownList($model,'tipo',
	                	Constants::$factura_tipo,
	                	array('options' => array(
	                		$factura ? $factura->tipo : 0 => 
	                			array(
	                				'selected'=>true
	                			)
	                		)
	                	) 
	                ); 
	            ?>
			</label>
		</div>
		<div class="medium-6 columns">
			<label>Calle
				<?php 
	                echo $form->textField($model,'calle', array(
	                        'placeholder'   => 'Calle',
	                        'value' 		=> $factura ? $factura->calle : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
	</div>


	<div class="row">
		<div class="medium-3 columns">
			<label>No. Exterior
				<?php 
	                echo $form->textField($model,'ext_numero', array(
	                        'placeholder'   => 'No. Exterior',
	                        'value' 		=> $factura ? $factura->ext_numero : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
		<div class="medium-3 columns">
			<label>No. Interior
				<?php 
	                echo $form->textField($model,'int_numero', array(
	                        'placeholder'   => 'No. Interior',
	                        'value' 		=> $factura ? $factura->int_numero : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
		<div class="medium-6 columns">
			<label>Código Postal
				<?php 
	                echo $form->textField($model,'codigo_postal', array(
	                        'placeholder'   => 'Código Postal',
	                        'value' 		=> $factura ? $factura->codigo_postal : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
	</div>

	<div class="row">
		<div class="medium-6 columns">
			<label>Estado
				<?php 
	                echo $form->dropDownList($model,'estado',
	                	Constants::$states,
	                	array('options' => array(
	                		$factura ? $factura->estado : 0 => 
	                			array(
	                				'selected'=>true
	                			)
	                		)
	                	) 
	                ); 
	            ?>
			</label>
		</div>
		<div class="medium-6 columns">
			<label>Ciudad
				<?php 
	                echo $form->textField($model,'ciudad', array(
	                        'placeholder'   => 'Ciudad',
	                        'value' 		=> $factura ? $factura->ciudad : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
	</div>

	<div class="row">
		<div class="small-6 medium-6 columns">
			<label>Delegación
				<?php 
	                echo $form->textField($model,'delegacion', array(
	                        'placeholder'   => 'Delegación',
	                        'value' 		=> $factura ? $factura->delegacion : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
		<div class="medium-6 columns">
			<label>Colonia
				<?php 
	                echo $form->textField($model,'colonia', array(
	                        'placeholder'   => 'Colonia',
	                        'value' 		=> $factura ? $factura->colonia : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
	</div>

	<div class="row">
		<div class="small-6 medium-6 columns">
			<label>Teléfono
				<?php 
	                echo $form->textField($model,'telefono', array(
	                        'placeholder'   => 'Teléfono',
	                        'value' 		=> $factura ? $factura->telefono : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
		<div class="medium-6 columns">
			<label>Correo
				<?php 
	                echo $form->textField($model,'correo', array(
	                        'placeholder'   => 'Correo',
	                        'value' 		=> $factura ? $factura->correo : null,
	                    )
	                ); 
	            ?>
			</label>
		</div>
	</div>

	<div class="row">
		<div class="medium-6 columns"></div>
		<div class="medium-6 columns">
			<?php 
                echo CHtml::submitButton('sendDatos', array(
                        'class' => 'button expanded m-t-40',
                        'value' => 'Guardar datos',
                        'id'    => 'send_factura'
                    )
                ); 
            ?>
		</div>
	</div>
<?php $this->endWidget(); ?>