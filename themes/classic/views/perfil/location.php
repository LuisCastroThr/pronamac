<div class="subsection-step generate-address" style="display: none;">
	<p class="h2-text">Ingresa dirección de envío</p>
	<?php 
        $model = new LocationForm;
        $form = $this->beginWidget('CActiveForm', array(
            'id'                    => 'location-form',
            'enableClientValidation'=> true,
            'clientOptions'         => array(
                'validateOnSubmit'  => true,
                'class'             => 'form-general'
            )
        )); 
    ?>
		<div class="row">
			<div class="medium-6 columns">
				<label>Calle
					<?php 
		                echo $form->textField($model,'calle', array(
		                        'placeholder'   => 'Calle',
		                        'value' 		=> $location ? $location->calle : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
			<div class="medium-3 columns">
				<label>No. Exterior
					<?php 
		                echo $form->textField($model,'ext_numero', array(
		                        'placeholder'   => 'No. Exterior',
		                        'value' 		=> $location ? $location->ext_numero : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
			<div class="medium-3 columns">
				<label>No. Interior
					<?php 
		                echo $form->textField($model,'int_numero', array(
		                        'placeholder'   => 'No. Interior',
		                        'value' 		=> $location ? $location->int_numero : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
		</div>

		<div class="row">
			<div class="medium-6 columns">
				<label>Código Postal
					<?php 
		                echo $form->textField($model,'codigo_postal', array(
		                        'placeholder'   => 'Código Postal',
		                        'value' 		=> $location ? $location->codigo_postal : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
			<div class="medium-6 columns">
				<label>Estado
					<?php 
		                echo $form->dropDownList($model,'estado',
		                	Constants::$states,
		                	array('options' => array(
		                		$location ? $location->estado : 0 => 
		                			array(
		                				'selected'=>true
		                			)
		                		)
		                	) 
		                ); 
		            ?>
				</label>
			</div>
		</div>

		<div class="row">
			<div class="medium-6 columns">
				<label>Ciudad
					<?php 
		                echo $form->textField($model,'ciudad', array(
		                        'placeholder'   => 'Ciudad',
		                        'value' 		=> $location ? $location->ciudad : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
			<div class="small-6 medium-6 columns">
				<label>Delegación
					<?php 
		                echo $form->textField($model,'delegacion', array(
		                        'placeholder'   => 'Delegación',
		                        'value' 		=> $location ? $location->delegacion : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
		</div>

		<div class="row">
			<div class="medium-6 columns">
				<label>Colonia
					<?php 
		                echo $form->textField($model,'colonia', array(
		                        'placeholder'   => 'Colonia',
		                        'value' 		=> $location ? $location->colonia : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
			<div class="medium-6 columns">
				<label>Teléfono
					<?php 
		                echo $form->textField($model,'telefono', array(
		                        'placeholder'   => 'Teléfono',
		                        'value' 		=> $location ? $location->telefono : null,
		                    )
		                ); 
		            ?>
				</label>
			</div>
		</div>

		<div class="row">
			<div class="medium-6 columns"></div>
			<div class="medium-6 columns">
				<?php 
	                echo CHtml::submitButton('send', array(
	                        'class' => 'button expanded m-t-40',
	                        'value' => 'Guardar Dirección',
	                        'id'    => 'send_location'
	                    )
	                ); 
	            ?>
			</div>
		</div>
	<?php $this->endWidget(); ?>
</div>