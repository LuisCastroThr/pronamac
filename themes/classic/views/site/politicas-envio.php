<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Política de Envíos</h1>
		
		<ul>
			<li>Cambio de precios sin previo aviso. </li>
			<li>No hay cambios físicos, devoluciones ni cancelaciones.</li>
			<li>El cliente se compromete a proporcionar una dirección válida localizable donde pueda entregarse el pedido. </li>
			<li>Los gastos de envío corren por cuenta del cliente, quien proporcionara el nombre de la paquetería, dirección y número de convenio en caso de existir.</li>
			<li>Una vez que el pedido está procesado y con el pago aplicado. El producto debe ser entregado en un máximo de 5 días hábiles después de que usted reciba la notificación de que está lista su orden de compra.</li>
			<li>Al momento de la entrega en el domicilio, le será solicitada a la persona que reciba la mercancía una identificación oficial (credencial de elector, pasaporte, forma migratoria FM2 o licencia de conducir).</li>
		</ul>
		
		<h2 class="h2-text">Pagos con depósito o transferencia</h2>
		<ul>
			<li>En esta modalidad los productos que se encuentran en existencia se envían generalmente en un máximo de 5 días hábiles (Lunes a Viernes) una vez comprobado su depósito y liberado su pedido.</li>
			<li>Para agilizar la liberación es importante que notifique su pago.</li>
			<li>Por correo: cotizaciones4@pronamac.com</li>
			<li>Comuníquese a (55) 5698-0430</li>
		</ul>


		<h2 class="h2-text">Tiempo de Entrega</h2>
		<ul>
			<li>El tiempo de entrega de los productos en existencia depende de la liberación de su pedido según el método de pago y el tipo de envío seleccionado.</li>
			<li>Todos los envíos facturados antes de las 12:00 horas, iniciarán el proceso de entrega ese mismo día, de lo contrario se realizara al siguiente día hábil. </li>
		</ul>

	</div>
</div>
