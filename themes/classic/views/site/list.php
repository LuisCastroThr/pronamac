<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">

		<h1 class="h1-text">Prevención de Infecciones</h1>

		<div class="row expanded">
			<div class="table-price">
				<div class="tr-table row column">
					<div class="small-2 columns"><strong>Código</strong></div>
					<div class="small-4 columns"><strong>Presentación</strong></div>
					<div class="text-center small-3 columns"><strong>Precio</strong></div>
					<div class="small-3 columns text-center"><strong>Cantidad</strong></div>
				</div>

				<div class="tr-table row column">
					<div class="small-6 columns">Texto</div>
					<div class="text-center small-3 columns"><span class="price">$2312</span></div>
					<div class="small-3 columns">
						<div class="select-product">
							<button type="button" name="button" class="small-3 columns select-number">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</button>
							<input class="small-6 columns input_count" type="text" name="" value="0" placeholder="0" data-id="">
							<button type="button" name="button" class="small-3 columns select-number">
								<i class="fa fa-plus" aria-hidden="true"></i>
							</button>
						</div>
					</div>
				</div>

				<div class="tr-table row column">
					<button type="button" name="button" class="button float-right add_cart_mlt">Agregar al carrito</button>
				</div>

			</div>


		</div>
	</div>
</div>
