<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Aviso de Privacidad</h1>

	<h2 class="h2-text">Responsable de la protección de sus datos personales</h2>
	<p>Con fundamento en los artículos 15 y 16 de la Ley Federal de Protección de Datos Personales en Posesión de Particulares hacemos de su conocimiento que Proveedora Nacional de Material de Curación, S.A. de C.V., con domicilio en C. 5 de Febrero No. 809 Col. Álamos, Del. Benito Juárez, Ciudad de México, C.P. 03400, es responsable del tratamiento (uso) de sus datos personales.</p>
	
	<br>
	<h2 class="h2-text">¿Para qué fines recabamos y utilizamos sus datos personales?</h2>
	<p>Sus datos personales serán utilizados para las siguientes finalidades: </p>
	<ul>
		<li>Proveer los productos solicitados.</li>
		<li>Proveer los servicios solicitados y/o contratados.</li>
		<li>Responder a sus requerimientos de información, atención y servicio. </li>
		<li>Evaluar la calidad del servicio que le brindamos.</li>
		<li>Archivo de registros y expediente de la relación contractual para seguimiento de servicios futuros. </li>
		<li>Gestión financiera, facturación y cobro.</li>
	</ul>	

	<p>Usted puede conocer los términos y alcances de nuestro Aviso de Privacidad integral solicitándolo a nuestro Departamento de Protección de Datos en: ventas_pronamac@prodigy.net.mx</p>
	

	<img width="200px" title="Material de curacion" alt="Material de curacion" class="float-right img-statics m-l" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/general/codigo-qr.png" alt="">

	<p>Si después de haber ejercido sus Derechos de Protección de Datos ante Proveedora Nacional de Material de Curación, S.A. de C.V. por medio de los mecanismos establecidos en este Aviso de Privacidad, considera que su derecho de protección de datos personales ha sido lesionado por alguna conducta u omisión de nuestra parte; o cuenta con evidencia de que en el tratamiento de sus datos personales existe alguna violación a las disposiciones previstas en la LFPDPPP, le invitamos a ponerse en contacto nuevamente con nosotros para agotar todos los procedimientos internos a efecto de satisfacer plenamente su solicitud. De no ser el caso, usted podrá interponer  la queja correspondiente ante el IFAI. Para mayor información visite:  www.ifai.org.mx</p>


	</div>
</div>
