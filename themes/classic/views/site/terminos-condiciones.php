<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Términos y condiciones de compra</h1>
	
		<p>Bienvenido al sitio web de compras en línea www.pronamac.com propiedad de Proveedora Nacional de Material de Curación, S.A. de C.V. con domicilio en C. 5 de Febrero No. 809, Col. Álamos, Del. Benito Juárez, Ciudad de México, C.P. 03400.</p>
		<p>Toda transacción realizada a través de sitio, se sujetará a los términos y condiciones aquí expresados, por lo anterior al ingresar y/o hacer uso de la misma como usuario, acepta los términos y condiciones aquí expresados, y se obliga a revisarlos con detenimiento. Asimismo, dicho usuario, acepta y reconoce que al confirmar su compra y finalizar el proceso de pago, se obliga a liquidar el costo total de los artículos solicitados, así como sus impuestos y/o gastos derivados aplicables, y que sus datos de facturación proporcionados a www.pronamac.com, son correctos y verdaderos.</p>
		<p>Estos Términos y Condiciones podrán ser modificados en cualquier momento por Proveedora Nacional de Material de Curación, S.A. de C.V., por lo que el cliente deberá realizar consultas periódicas a los mismos para conocer las disposiciones vigentes al momento de su compra.</p>
	</div>
</div>
