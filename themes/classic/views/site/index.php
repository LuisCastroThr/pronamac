<?php include 'templates/gallery.php'; ?>


<section class="section-index suppliers">
	<div class="row column">
		<h2 class="text-center title-green">Principales Proveedores</h2>

		<div class="text-center small-6 medium-3 large-3 columns">
			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>2)); ?>">
				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/3m.jpg" alt="">
			</a>
		</div>
		<div class="text-center small-6 medium-3 large-3 columns">
			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>16)); ?>">
				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/bd.jpg" alt="">
			</a>
		</div>
		<div class="text-center small-6 medium-3 large-3 columns">
			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>5)); ?>">
				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/bsn-medical.jpg" alt="">
			</a>
		</div>
		<div class="text-center small-6 medium-3 large-3 columns">
			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>6)); ?>">
				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/convatec.jpg" alt="">
			</a>
		</div>
		<div class="text-center small-6 medium-3 large-3 columns">
			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>8)); ?>">
				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/Hollister.jpg" alt="">
			</a>
		</div>
		<div class="text-center small-6 medium-3 large-3 columns">
<!-- 			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>17)); ?>">
 -->				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/asepsis.png" alt="">
<!-- 			</a>
 -->		</div>
		<div class="text-center small-6 medium-3 large-3 columns">
			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>18)); ?>">
				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/Smith-nephew.jpg" alt="">
			</a>
		</div>
		<div class="text-center small-6 medium-3 large-3 columns">
			<a href="<?php echo Yii::app()->createUrl('site/Products',array('proveedor_id'=>15)); ?>">
				<img width="150" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/suppliers/ansell.jpg" alt="">
			</a>
		</div>
	</div>
</section>
