<script type="text/javascript">
	function imgError(image) {
	    image.onerror = "";
	    image.src = Pronamac.Object.urlTest+"/themes/classic/src/product/no-foto.jpg";
	    return true;
	}
</script>

<div class="wrapper-general row">
	<div class="hide-for-small-only small-12 medium-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-12 medium-9 large-9 columns description-product">

		<h1 class="h1-text"><?php echo $titleName; ?></h1>

		<div class="row expanded">
			<div class="small-12 medium-5 large-5 columns">
				<img class="thumbnail" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/product/<?php echo $presentaciones[0]->image; ?>" data-open="gallery" onerror="imgError(this);">
			</div>
			<?php if( isset($presentaciones[0]) ): ?>
				<div class="small-12 medium-7 large-7 columns">
					<p>Tipo de producto: <strong><?php echo $presentaciones[0]->nombre; ?></strong></p>
					<p>Proveedor: <strong><?php echo $titleProveedor; ?></strong></p>
					<p>
						<?php echo $presentaciones[0]->descripcion_gral; ?>
					</p>
				</div>
			<?php endif; ?>
		</div>

		<div class="table-price">
			<div class="tr-table row column">
				<div class="small-3 columns">Código</div>
				<div class="text-center small-5 columns">Presentación</div>
				<div class="text-center small-4 columns">Unidad de Venta</div>
<!-- 				<div class="text-center small-2 columns">Precio</div>
				<div class="text-center small-2 columns">Cantidad</div> -->
			</div>

			<?php foreach ($presentaciones as $presentacion): ?>
				<div class="tr-table row column">
					<div class="small-3 columns">
							<?php if(!empty($presentacion->codigo)) : ?>
								<b><?php echo $presentacion->codigo; ?> </b>
							<?php else: ?>
								N/D
							<?php endif; ?>
					</div>
					<div class=" text-center small-5 columns truncate">
						<?php echo $presentacion->presentacion; ?>
					</div>

					<div class="text-center small-4 columns">
						<?php echo $presentacion->unidad; ?>
					</div>

<!-- 					<div class="text-center small-2 columns">
						<?php //if(!Yii::app()->user->isGuest): ?>
							<span class="price">$ <?php //echo number_format($presentacion->precio, 2, '.', ','); ?></span>
						<?php //endif; ?>
					</div> -->

					<!-- <div class="small-2 columns">
						<?php //if(!Yii::app()->user->isGuest): ?>
							<div class="select-product">
								<button type="button" name="button" class="small-3 columns select-number">
									<i class="fa fa-minus" aria-hidden="true"></i>
								</button>
								<input class="small-6 columns input_count" type="text" name="" value="0" placeholder="0" data-id="<?php //echo $presentacion->id; ?>">
								<button type="button" name="button" class="small-3 columns select-number">
									<i class="fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>
						<?php //endif; ?>
					</div> -->
				</div>
			<?php endforeach; ?>

			<?php if(!Yii::app()->user->isGuest): ?>
				<div class="tr-table row column">
					<button type="button" name="button" class="button float-right add_cart_mlt">Agregar al carrito</button>
				</div>
			<?php endif; ?>
		</div>


	</div>
</div>
