<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Quiénes Somos</h1>
		<p><strong>Proveedora Nacional de Material de Curación, S.A. de C.V.</strong>, creada en el año 1986, es una empresa mexicana, dedicada a la distribución de material de curación.

		<p>Nuestro profesionalismo está respaldado en la calidad de los productos, la asistencia hacia nuestros clientes, así como en la experiencia y responsabilidad de todo nuestro personal.</p>

		<p>Gracias a la eficiencia demostrada en el servicio, se generan alianzas con los fabricantes mas importantes, lo cual lleva al lanzamiento de nuevos proyectos de colaboración conjunta para los clientes.</p>

		<p>Actualmente, nuestra empresa cuenta con su propia marca “Asepsis Care”, misma que consta de Kits
		diseñados para unificar los procesos y materiales que debe utilizarse durante la instalación, mantenimiento y
		retiro de los accesos vasculares, así como en otros procesos, garantizando la seguridad al usuario final,
		cumpliendo con los requisitos establecidos de acuerdo a la normatividad de los sistemas de salud (NOM 045
		y NOM 022).</p>

		<p>Hoy en día brindamos nuestros servicios a toda la Republica Mexicana, contando con 3 ubicaciones estratégicas y una filial.</p>
		<ul>
			<li>Ciudad de México.</li>
			<li>Jalisco.</li>
			<li>Nuevo León.</li>
			<li>Guanajuato (Promehbsa).</li>
		</ul>

		<h2 class="h2-text">Misión</h2>
		<p>Comercializar a nivel nacional productos, materiales e insumos médicos de la mas alta calidad, destacando la actitud de servicio y atención personalizada con calidez humana, orientada a la satisfacción de nuestros clientes.</p>

		<h2 class="h2-text">Visión</h2>
		<p>Convertirnos en un distribuidor líder a nivel nacional e internacional en el sector salud, mejorando continuamente nuestros estándares de calidad e innovando nuestros procesos de acuerdo a las necesidades de nuestros clientes, a través del desarrollo de nuestro talento humano.</p>
	</div>
</div>
