<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>

	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Únete a nuestro equipo de trabajo !!!</h1>
		<p>En PRONAMAC, nuestros colaboradores son la base del éxito obtenido, ya que se logra un sentido de pertenencia y esto se traduce en el actuar de cada uno de ellos. La clave esta en la gestión, motivación brindada y a la cultura organizacional compartida.</p>


		<p><strong>Beneficios:</strong></p>

		<ul>
			<li>Sueldo Competitivo.</li>
			<li>Excelente ambiente de trabajo.</li>
			<li>Horario flexible.</li>
			<li>Prestaciones Ley.</li>
			<li>Aprendizaje constante.</li>
		</ul>


		<h2 class="h2-text">¿Quieres ser parte de nuestro equipo?</h2>
		<p>Envía tu CV al correo <a href="mailto:claudia.morquecho@pronamac.com">claudia.morquecho@pronamac.com</a></p>
	</div>
</div>
