<!-- <ul class="categories menu vertical mn_cat" data-accordion-menu>
    <li class="title-categories">Categorías</li>
    <?php 
        // $criteria = new CDbCriteria();
        // $criteria->select = 't.id, t.name';
        // $categorias = PncCategorias::model()->findAll($criteria);
        // foreach ($categorias as $categoria): 
    ?>
        <li>
            <a><span><?php //echo $categoria->name; ?></span></a>
            <?php 
                // $criteria = new CDbCriteria();
                // $criteria->select = 't.id_proveedor';
                // $criteria->distinct = true;
                // $criteria->addCondition('t.id_categoria = '.$categoria->id);
                // $prov = PncProductos::model()->findAll($criteria);
                // foreach ($prov as $prov): 
            ?>
                <ul class="menu vertical nested">
                    <li>
                        <a href="<?php //echo Yii::app()->createUrl('site/Products', 
                            // array(
                            //     'proveedor_id' => $prov->proveedor->id,
                            //     'categoria_id' => $categoria->id)
                            // ); 
                        ?>">
                            <?php //echo $prov->proveedor->name; ?>        
                        </a>
                    </li>
                </ul>
            <?php //endforeach; ?>
        </li>
    <?php //endforeach; ?>
</ul> -->


<ul class="categories menu vertical mn_pro" data-accordion-menu>
    <li class="title-categories">PROVEEDORES</li>
    <?php 
        $criteria = new CDbCriteria();
        $criteria->select = 't.id, t.name';
        $criteria->order = 't.name';
        $criteria->addCondition('t.id != 17');
        $proveedores = PncProveedores::model()->findAll($criteria);
        foreach ($proveedores as $proveedor): 

                $criteria = new CDbCriteria();
                $criteria->select = 't.id_categoria';
                $criteria->distinct = true;
                $criteria->order = 't.id_categoria';
                $criteria->addCondition('t.id_proveedor = '.$proveedor->id);
                $cat = PncProductos::model()->findAll($criteria);

                if(!empty($cat)):
    ?>
        <li><a href="#"><span><?php echo strtoupper($proveedor->name); ?></span></a>
            <?php 
                foreach ($cat as $cat): 
            ?>
                <ul class="menu vertical nested">
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('site/Products', 
                            array(
                                'categoria_id' => $cat->categoria->id,
                                'proveedor_id' => $proveedor->id)
                            ); 
                        ?>">
                            <?php echo strtoupper($cat->categoria->name); ?>
                        </a>
                    </li>
                </ul>
            <?php endforeach; ?>
        </li>
    <?php 
        endif;
    endforeach; ?>
</ul>
