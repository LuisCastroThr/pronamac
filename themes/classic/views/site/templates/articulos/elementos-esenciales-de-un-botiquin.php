<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">ELEMENTOS ESENCIALES DE UN BOTIQUÍN DE PRIMEROS AUXILIOS</h1>
		<p>Los elementos esenciales de un botiquín de primeros auxilios se pueden clasificar así:</p>

		<h2 class="h2-text">1. Antisépticos</h2>
		<p>Los antisépticos son substancias cuyo objetivo es la prevención de la infección evitando el crecimiento de los gérmenes que comúnmente están presente en toda lesión. Cuando se presentan individualmente en sobres que contienen pañitos húmedos con pequeñas cantidades de solución, se facilita su transporte y manipulación.</p>

		<h2 class="h2-text">2. Material de curación</h2>
		<p>El <a href="material-de-curacion.php">material de curación</a> es indispensable en <strong>botiquín de primeros auxilios</strong> y se utiliza para:</p>
		<ul>
			<li>Controlar hemorragias, limpiar, cubrir heridas o quemaduras.</li>
			<li>Prevenir la contaminación e infección.</li>
		</ul>

		<h2 class="h2-text">3. Instrumental y elementos adicionales</h2>
		<img width="300px" title="Elementos esenciales de un botiquin" alt="Elementos esenciales de un botiquin" class="float-right img-statics" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/articulos/elementos-esenciales-de-un-botiquin.jpg" alt="">
		<ul>
			<li>Tapabocas y Guantes Desechables</li>
			<li>PinzasBotiquín</li>
			<li>Tijeras</li>
			<li>Cuchillas</li>
			<li>Navajas</li>
			<li>Termometro Oral</li>
			<li>Ganchos de Nodriza</li>
			<li>Lupa</li>
			<li>Linterna</li>
			<li>Libreta y lápiz</li>
			<li>Caja de fósforos o encendedor</li>
			<li>Lista de Télefonos de Emergencia</li>
			<li>Gotero</li>
			<li>Manual o folleto de Primeros Auxilios</li>
		</ul>

		<p>Otras cosas que le pueden ser útiles son:</p>
		<ul>
			<li>Pañuelos desechables</li>
			<li>Toallitas húmedas</li>
			<li>Manta térmica</li>
			<li>Bolsas de Plástico</li>
			<li>Vasos desechables</li>
			<li>Cucharas</li>
			<li>Aguja e Hilo</li>
		</ul>

		<h2 class="h2-text">4. Medicamentos</h2>
		<p>El <strong>botiquín de primeros auxilios</strong> debe contener principalmente analgésicos, calmantes para aliviar el dolor causados por traumatismo y para evitar entre en estado de shock, sin embargo no debe usarse indiscriminadamente porque por su acción puede ocultar la gravedad de su lesión. Los principales analgésicos que se utiliza son de ácido acetilsalicilico y acetaminofen que en el mercado, puede encontrarse con diferentes nombres comerciales, estos también son antipiréticos (bajan la fiebre).</p>
		<p>Puedes consultar nuestros productos que tenemos, para que puedas <a href="productos.php">armar tu propio botiquín</a>.</p>
	</div>
</div>
