<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">CUIDA A TU CORAZÓN</h1>
		<img width="350px" title="Cuida tu corazon" alt="Cuida tu corazon" class="float-right img-statics m-l" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/articulos/cuida-tu-corazon.jpg" alt="">
		<p><b>Te mostramos algunas acciones que puedes llevar acabo para cuidar tu corazón:</b></p>
		<ul>
			<li>No fumar y evitar exponerse al humo del cigarro de segundas personas, esto reduce sufrir un ataque cardiaco.</li>
			<li>Hacer ejercicio para mantener un peso estable y protegernos contra las enfermedades.</li>
			<li>Controlar los niveles del colesterol, ya que el colesterol bajo no presenta síntomas solo hasta el momento de que ya está muy alto.</li>
			<li>Mantener un peso saludable, por lo que la obesidad aumenta el riesgo de padecer diabetes y ataques cardiacos.</li>
			<li>Comer alimentos que contengan hierro como la carne, huevos, lentejas, frijoles y hojas verdes, para evitar la anemia.</li>
			<li>Disminuye las sales, azucares y las grasa.</li>
			<li>Si es mamá, anime a sus hijos a que practiquen algún deporte y a que jueguen al aire libre.</li>
			<li>El perímetro abdominal se puede medir fácilmente con una cinta métrica. La persona debe estar de pie, con los pies juntos, los brazos a los lados y el abdomen relajado, a continuación, rodear su abdomen con la cinta métrica a la altura del ombligo y sin presionar hacer una inspiración profunda y al momento sacar el aire, medir el perímetro abdominal. </li>
		</ul>


		<h2 class="h2-text">La OMS establece los siguientes valores:</h2>
		<p>El valor máximo saludable en la mujer es de 80 cm.</p>
		<p>El valor máximo en hombres es de 90 cm.</p>
	</div>
</div>
