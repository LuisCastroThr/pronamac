<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Cuidado de heridas</h1>
		<p>Antes de saber qué hacer para que una herida no se infecte es necesario conocer qué es la infección de una herida. Una <strong>infección de herida</strong> ocurre cuando los microbios se instalan en la incisión de la piel. Estas bacterias se alojan en los tejidos impidiendo que la herida se cure y causando otros síntomas. La infección tanto puede suceder en heridas pequeñas o grandes, desde cuando no sean tratadas adecuadamente.</p>

		<h2 class="h2-text">Cómo saber si una herida está infectada</h2>
		<p>La infección puede salir en todos los <strong>tipos de heridas</strong>. En heridas provocadas por objetos punzantes, en cortes, desgarros o quemaduras. La infección es más común en incisiones más profundas como grandes quemaduras o heridas provocadas por mordeduras. Para saber si la herida del niño se ha infectado, se puede observar algunas señales:</p>
		<ul>
			<li>Heridas que no se sanan ni mejoran incluso tras un tratamiento.</li>
			<li>Heridas que duelen y que sangra fácilmente.</li>
			<li>Heridas que presentan un absceso con flujo o pus de un color raro o mal olor.</li>
			<li>Heridas que presentan áreas inflamadas y muy enrojecidas a su alrededor.</li>
			<li>Heridas doloridas y tibias, cuando tocadas.</li>
		</ul>

		<h2 class="h2-text">Cómo evitar la infección de una herida</h2>
		<img width="300px" title="Cuidado de heridas" alt="Cuidado de heridas" class="float-left img-statics" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/articulos/cuidado-de-heridas.jpg" alt="">
		<p>1. Utilizar <a href="productos.php">antisépticos</a> como la mercromina sobre las <strong>heridas de los niños</strong>. Está comprobado que estos productos pueden producir lo que se conoce como eczema de contacto, y si se utilizan en grandes superficies y durante mucho tiempo, es tóxico para el riñón.</p>
		<p>2. Utilizar alcohol para desinfectar la herida de los niños. Aparte de provocar mucho dolor, puede causar quemaduras y por lo tanto empeorar el cuadro clínico de la herida. La acción desinfectante y antiséptica de la clorhexidina dura más que la del alcohol.</p>
		<p>3. Utilizar povidona yodada en la herida de los niños. Está más orientada para los adultos. La clorhexidina es más efectiva en el caso de los niños.</p>
		<p>4. Utilizar agua oxigenada para desinfectar. Sólo sirve para limpiar la herida, del mismo modo que el suero fisiológico.</p>
	</div>
</div>
