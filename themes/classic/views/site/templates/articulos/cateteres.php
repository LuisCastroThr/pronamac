<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Tipos de catéteres</h1>

		<img width="250px" title="catéter" alt="catéter" class="float-left img-statics" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/articulos/cateter.jpg" alt="">

		<br>
		<br>
		<p>De acuerdo a la <strong>localización anatómica</strong> se denominan:<br>Catéter venoso periférico (CVP), catéter venoso periférico de línea media (CVPM), catéter central de inserción periférica (PICC) y catéter venoso central (CVC).</p>
		<br>
		<br>

		<h2 class="h2-text">Catéter venoso periférico (CVP)</h2>
		<p>El abordaje de la vía venosa se realiza con una cánula o catéter corto. Los <strong>catéteres venosos periféricos</strong> son los dispositivos más utilizados en la administración endovenosa de fluidos. Su uso está recomendado cuando la administración farmacológica no supera los seis días de tratamiento o cuando las sustancias a infundir no son vesicantes o hiperosmolares.</p>


		<h2 class="h2-text">Catéter venoso periférico de línea media (CVPM)</h2>
		<p>Tiene una longitud de 7 a 20 cm, se inserta en la <strong>fosa antecubital</strong>, situando la punta del catéter en el paquete vascular que se encuentra debajo de la axila. La permanencia es de dos a cuatro semanas, si no hay complicaciones.</p>

		<h2 class="h2-text">Catéter Central de Inserción Periférica (PICC)</h2>
		<p>Estos catéteres se insertan habitualmente, en <strong>venas cefálicas y basílicas</strong> de los miembros superiores, se puede utilizar cualquier acceso periférico con capacidad suficiente para alojarlo.</p>

		<h2 class="h2-text">Catéter Venoso Central (CVC)</h2>
		<p class="m-b">Consiste en canalizar el vaso venoso con un catéter o cánula larga. Se considera <strong>CVC</strong> cuando el extremo distal del mismo se ubica en vena cava superior, vena cava inferior o cualquier zona de la anatomía cardiaca, siendo esta última localización permitida sólo para el catéter Swan-Ganz, que se situará en arteria pulmonar.</p>

		<div class="m-b">
			<h3 class="h3-text">Artículos de Interés</h3>
			<ul>
				<li><a href="esponja-esteril-con-sulfato-de-gentamicina-y-colageno.php">Esponja estéril con sulfato de gentamicina y colágeno.</a></li>
				<li><a href="material-de-curacion.php">Material de curación.</a></li>
				<li><a href="elementos-esenciales-de-un-botiquin.php">Elementos escenciales de un botiquín.</a></li>
			</ul>
		</div>

		<div>
			<h3 class="h3-text">Productos relacionados</h3>
			<ul>
				<li><a href="">Cateter Insyten Neonatal Liso</a></li>
				<li><a href="">Cateter Intima</a></li>
			</ul>
		</div>

	</div>
</div>
