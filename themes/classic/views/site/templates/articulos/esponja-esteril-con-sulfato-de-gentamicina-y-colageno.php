<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">ESPONJA ESTÉRIL CON SULFATO DE GENTAMICINA Y COLÁGENO</h1>
		<p>La esponja de colágeno bovino con gentamicina es una combinación de dos sustancias para la aplicación local de antibióticos teniendo un efecto hemostático promoviendo la cicatrización y provocando altas concentraciones locales del antibiótico en el sitio de heridas quirúrgicas o traumáticas.</p>
		<p>Es un implante estéril que contiene sulfato de <strong>GENTAMICINA</strong> el cual brinda un rápido efecto bactericida dependiente de la concentración.</p>
		<p>Cada esponja contiene Sulfato de Gentamicina equivalente a 1.3 / 2.8 mg de Gentamicina.</p>

		<h2 class="h2-text">Propiedades</h2>
		<img width="300px" title="Esponja esteril" alt="Esponja esteril" class="float-right img-statics m-l" src="src/articulos/esponja-esteril.jpg" alt="">
		<ul>
			<li>Características del Colágeno.</li>
			<li>Altamente purificada.</li>
			<li>Derivada de tendón de Aquiles bovino.</li>
			<li>Nativo (No es enzimáticamente diferido o degradado).</li>
			<li>Fibrilar (contiene pequeños grupos de fibras).</li>
			<li>Tipo I insoluble</li>
			<li>Se absorbe totalmente en 1-2 semanas, dependiendo el sitio de perfusión.</li>
			<li>La difusión de la Gentamicina es homogénea en el sitio de la infección.</li>
			<li>Las concentraciones locales de sulfato de Gentamicina alcanzan un pico alrededor de las 24 horas.</li>
			<li>Nivel Local 100 µg/ml a 3000 µg/ml</li>
			<li>Nivel Sérico 1- 2 µg/ml</li>
			<li>Mínima exposición sistémica (nivel plasmático < 2 mg/ml).</li>
			<li>Su uso puede ser como tratamiento auxiliar en las infecciones de los huesos y del tejido blando debido a bacterias sensibles a la gentamicina o como profiláctico para evitar infecciones en pacientes de alto riesgo.</li>
		</ul>
		<p class="text-center"><strong>“El primer objetivo de una profilaxis antimicrobiana es el de prevenir la invasión y crecimiento de microorganismos con una cantidad adecuada del medicamento en el sitio operatorio”.</strong></p>

	</div>
</div>