<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">Tipos de catéteres</h1>

		<img width="300px" title="Bacteriemia cero" alt="Bacteriemia cero" class="float-left img-statics" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/articulos/bacteriemia-cero.jpg" alt="">

		<br>
		<p>El <strong>uso de dispositivos intravenosos</strong> incrementa el riesgo para el paciente de contraer <a href="cuidados-de-heridas.php">infecciones</a> dentro del mismo hospital, si no se cumplen las medidas preventivas para su colocación y/o las condiciones de salud no son las óptimas. Las <strong>infecciones del torrente sanguíneo</strong> son causa de altas estancias, mortalidad, dolor y sufrimiento para los pacientes.</p>
		<br>
		<br>
		<br>

		<h2 class="h2-text">Acciones para reducir la infección nosocomial:</h2>
		<h3 class="h3-list">1. Vigilar la calidad del agua.</h3>
		<ul>
			<li>Verificar la concentración de cloro en el agua de cisternas.</li>
			<li>Clorar en caso necesario.</li>
			<li>Muestrear servicios diferentes para asegurar la calidad del agua.</li>
			<li>Limpiar las cisternas.</li>
		</ul>

		<h3 class="h3-list">2. Higiene correcta de manos.</h3>
		<ul>
			<li>Con agua y jabón o con productos a base de alcohol.</li>
			<li>Antes de la inserción o mantenimiento del catéter.</li>
			<li>El uso de guantes no sustituye el lavado de manos.</li>
		</ul>

		<h3 class="h3-list">3. Preparación de la piel.</h3>
		<ul>
			<li>Antes de la inserción del catéter y para el cuidado.</li>
			<li>Preparación con clorhexidina o yodopovina.</li>
			<li>Dejar secar el antiséptico al aire libre y no retirarlo.</li>
			<li>Prefiera envases no rellenables para estos productos.</li>
		</ul>

		<h3 class="h3-list">4. Medidas máximas de barrera durante la inserción de los CVC.</h3>
		<ul>
			<li>Higiene de manos.</li>
			<li>Uso de cubre bocas.</li>
			<li>Utilice bata estéril y campos quirúrgicos.</li>
			<li>Utilice guantes estériles.</li>
		</ul>

		<h3 class="h3-list">5. Permitir la manipulación de los dispositivos intravasculares sólo por personal calificado.</h3>
		<ul>
			<li>Nom-045-SSA2-2005, Para la vigilancia epidemiológica, prevención y control de infecciones nosocomiales.</li>
			<li>Equipo de enfermeras de terapia intravenosa.</li>
		</ul>

		<h3 class="h3-list">6. Retiro de CVC  innecesarios.</h3>
		<ul>
			<li>Valorar permanentemente la justificación del CVC.</li>
			<li>Racionalizar el uso de soluciones y medicamentos IV.</li>
			<li>Sellar los catéteres que no requieran su uso en pacientes estables.</li>
		</ul>
	</div>
</div>
