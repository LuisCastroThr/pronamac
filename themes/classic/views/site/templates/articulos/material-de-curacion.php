<div class="wrapper-general row">
	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/categories'); ?>
	</div>
	<div class="small-9 large-9 columns">
		<h1 class="h1-text">MATERIAL DE CURACIÓN</h1>
		<img width="250px" title="Material de curacion" alt="Material de curacion" class="float-left img-statics" src="<?php echo Yii::app()->theme->baseUrl; ?>/src/articulos/material-de-curacion.jpg" alt="">

		<br>
		<p>El material de curación son aquellos instrumentos y productos que se utilizan para curar a una persona antes, durante o después de sufrir una herida. </p>
		<p>Son utilizados para controlar hemorragias, limpiar y cubrir heridas o quemaduras y prevenir la contaminación e infección.</p>
		<br>
		<br>
		<br>

		<h2 class="h2-text">Elementos principales del Material de Curación</h2>

		<div class="table-text">
			<div class="tr-table row column">
				<div class="small-3 columns"><strong>Gasas</strong></div>
				<div class="text-left small-9 columns">Para cubrir las heridas de la piel y vienen en varios tamaños para su uso en heridas de diferentes tamaños.</div>
			</div>
			<div class="tr-table row column">
				<div class="small-3 columns"><strong>Guantes</strong></div>
				<div class="text-left small-9 columns">Para auxiliar un accidentado es cada vez más común para evitar el contagio de enfermedades.</div>
			</div>
			<div class="tr-table row column">
				<div class="small-3 columns"><strong>Vendas</strong></div>
				<div class="text-left small-9 columns">
					<ul>
						<li>Vendas de gasa, usadas para sujetar apósitos en su sitio. Su tejido suelto permite una buena ventilación, pero no se usan para ejercer presión directa sobre una herida ni para sujetar articulaciones.</li>
						<li>Vendas elásticas, que se adaptan a la forma del cuerpo. Se usan para asegurar apósitos y para lesiones del tejido blando.</li>
						<li>Vendas de crepé, usadas para dar soporte firme a lesiones en articulaciones.</li>
					</ul>
				</div>
			</div>
			<div class="tr-table row column">
				<div class="small-3 columns"><strong>Agua oxigenada</strong></div>
				<div class="text-left small-9 columns">Se utiliza para limpiar heridas y desinfectarlas. También en productos de belleza y tintes.</div>
			</div>
		</div>
	</div>
</div>
