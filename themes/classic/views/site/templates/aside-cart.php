<div class="categories cart">
    <p class="title-categories">Resumen de Compra</p>
	<div class="cart carrito_div_detail">
		<ul>
			<li>Subtotal</li>
			<li class="price car_subtotal"><?php echo $detalle['subTotal']; ?></li>
		</ul>
		<!-- <ul>
			<li>Envío</li>
			<li class="car_envio"><?php echo $detalle['envio']; ?></li>
		</ul> -->
		<ul>
			<li>IVA (16%)</li>
			<li class="car_iva"><?php echo $detalle['iva']; ?></li>
		</ul>
		<ul>
			<li>Total</li>
			<li class="price car_total"><?php echo $detalle['total']; ?></li>
		</ul>
		<!-- <p class="text-center message-small"><small>No más de 10 Kg, sin cristalería y sin soluciones</small></p> -->
	</div>
	<?php if(Yii::app()->controller->action->id == 'CartStepTwo'): ?>
		<a id="check_send_cart" href="#" data-url="<?php echo Yii::app()->createUrl('pncCarrito/NumberAccount'); ?>" class="button expanded">Realizar Pago</a>
	<?php else: ?>
		<a href="<?php echo Yii::app()->createAbsoluteUrl('pncCarrito/CartStepTwo'); ?>" class="button expanded">Siguiente</a>
	<?php endif; ?>
</div>