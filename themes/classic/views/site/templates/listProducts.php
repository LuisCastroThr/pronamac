<div class="hide-for-small-only small-12 medium-6 large-4 columns m-b box-product">
	<div class="product">
		<div class="img-product">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/product/<?php echo $data->image; ?>" alt="" onerror="imgError(this);">
		</div>
		<div class="description-product">
			<h2 class="h2-product truncate"><?php echo $data->nombre; ?></h2>
			<a href="<?php echo Yii::app()->createUrl('site/view_product',array('id'=>$data->id)); ?>" class="button expanded">Ver producto</a>
		</div>
	</div>
</div>