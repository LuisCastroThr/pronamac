<div class="container-gallery">
    <div class="orbit gallery-home parte1">
        
            <li class="orbit-slide is-active" data-slide="0" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/bg-welcome.png" style="opacity:.9; height: 500px; width: auto;">

                <div class="orbit-caption first-caption second">
                    <h1 class="title-orbit">“Una empresa al servicio de la Salud”</h1>
                </div>
            </li>
    </div>

    <div class="orbit gallery-home" role="region" 
                                    data-orbit 
                                    data-options="animInFromLeft:fade-in; 
                                                  animInFromRight:fade-in; 
                                                  animOutToLeft:fade-out; 
                                                  animOutToRight:fade-out;
                                                  timer_speed: 10000, animation_speed: 100,">
        <ul class="orbit-container" tabindex="0">
            <button class="orbit-previous" aria-label="previous" tabindex="0"><span class="show-for-sr">Previous Slide</span>◀</button>
            <button class="orbit-next" aria-label="next" tabindex="0"><span class="show-for-sr">Next Slide</span>▶</button>
            
            <li class="orbit-slide is-active" data-slide="0" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/bg-welcome.png" style="opacity:.9; height: 500px; width: auto;">

                <div class="orbit-caption first-caption second">
                    <h1 class="title-orbit">“Una empresa al servicio de la Salud”</h1>
                </div>
            </li>

            <li class="orbit-slide is-active" data-slide="0" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/bg-welcome.png" style="opacity:.9; height: 500px; width: auto;">

                <div class="orbit-caption first-caption">
                    <h1 class="title-orbit">Bienvenidos a pronamac</h1>
                    <h2>Somos una empresa altamente competitiva con mas de 31 años de experiencia en el mercado de material de curación. </h2>
                    <p>Ofrecemos distribución directa de material de alto valor agregado, por medio de las marcas mas prestigiadas del sector salud. </p><br><p>Damos servicio a Hospitales, Institutos, Clínicas, Laboratorios y Pacientes a nivel nacional (Sector Gubernamental y Privado). </p><br><p>Brindamos capacitación y promoción a todos los servidores de la salud para mantener a la vanguardia los servicios de nuestros clientes.</p>
                </div>
            </li>

            <li class="orbit-slide" data-slide="1" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/Kit.jpg" style="opacity:.9; height: 500px; width: auto;">

                <div class="orbit-caption first-caption second">
                    <h2 class="title-orbit">KIT ASEPSIS CARE</h2><br>
                    <p>Kits diseñados con el objetivo de brindar una atención de calidad a pacientes que requieren terapias vasculares y/u otros procesos.</p>
                    <br>
                    <p>Se garantiza al usuario final seguridad, ya que evita riesgo de infecciones ocasionadas por una inadecuada reutilización del producto.</p>
                </div>
            </li>


            <li class="orbit-slide" data-slide="1" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/chloraprerp.jpg" style="opacity:.9; height: 500px; width: auto;">
                <div class="orbit-caption first-caption second">
                    <h2 class="title-orbit">CHLORAPREP</h2><br>
                    <p>Antiséptico para asepsia cutánea.</p><br>
                    <p>Gluconato de Clorhexidina al 2% y Alcohol isopropílico al 70%.</p><br>
                    <p>Línea completa de aplicadores para la prevención de infecciones desde donde se originan.</p>
                </div>
            </li>

            <li class="orbit-slide" data-slide="1" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/agujas.jpg" style="opacity:.9; height: 500px; width: auto;">
                <div class="orbit-caption first-caption second">
                    <h2 class="title-orbit">AGUJAS DE BIOPSIA</h2>
                    <br>
                    <p>Amplia gama de agujas para biopsia de tejidos blandos y médula ósea en las que usted puede confiar para obtener muestras precisas en todo momento, ofrecen una completa selección con funciones avanzadas para extraer fragmentos de hueso y líquidos de la médula ósea, así como muestras de tejido.</p>
                </div>
            </li>

            <li class="orbit-slide" data-slide="1" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/oxivir.jpg" style="opacity:.9; height: 500px; width: auto;">
                <div class="orbit-caption first-caption second">
                    <h2 class="title-orbit">OXIVIR</h2>
                    <br>
                    <p>Desinfectante de amplio espectro listo para usar, basado en la tecnología patentada de peróxido de hidrógeno (AHP) para proporcionar un rendimiento de limpieza rápido y efectivo.</p>
                    <br>
                    <p>Virucida, bactericida, tuberculocida, fungicida y desinfectante de contacto no alimentario.</p>
                </div>
            </li>

            <li class="orbit-slide" data-slide="1" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/posiflush.jpg" style="opacity:.9; height: 500px; width: auto;">
                <div class="orbit-caption first-caption second">
                    <h2 class="title-orbit">POSIFLUSH</h2><br>
                    <p>Jeringas de solución salina y heparina prellenadas.</p>
                    <br>
                    <p>Diseñadas para eliminar el reflujo de sangre inducido por jeringa, disminuir el riesgo de daño del catéter y para contribuir a la mejora de la práctica clínica.</p>
                </div>
            </li>

            <li class="orbit-slide" data-slide="1" style="position: relative; top: 0px; display: block;" aria-live="polite">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/home/insyte.jpg" style="opacity:.9; height: 500px; width: auto;">
                <div class="orbit-caption first-caption second">
                    <h2 class="title-orbit">INSYTE AUTOGUARD</h2><br>
                    <p>Tecnología de activación del sistema de seguridad. </p><br>
                    <p>Confirmación inmediata de inserción en la vía y mayor éxito del primer pinchazo.</p>
                </div>
            </li>
        </ul>
    </div>
</div>