<div class="reveal gallery-modal" id="gallery" data-reveal>

    <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container" style="height: 540px;">
            <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
            <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
            <li class="is-active orbit-slide">
                <img class="orbit-image" src="src/product/product.png" alt="Space">
            </li>
            <li class="orbit-slide">
                <img class="orbit-image" src="src/product/product.png" alt="Space">
            </li>
            <li class="orbit-slide">
                <img class="orbit-image" src="src/product/product.png" alt="Space">
            </li>
            <li class="orbit-slide">
                <img class="orbit-image" src="src/product/product.png" alt="Space">
            </li>
        </ul>
    </div>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
