<footer>
    <div class="row column">
        <div class="small-12 medium-4 large-4 columns">
            <h4 class="text-center">Contáctanos</h4>
            <?php
                $model=new ContactForm;
                $form=$this->beginWidget('CActiveForm',
                    array(
                        'id'=>'contact-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )
                );
            ?>
                <div class="row">
                    <?php echo $form->textField($model,'name', array('placeholder' => 'Nombre')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textField($model,'phone', array('placeholder' => 'Teléfono')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textField($model,'email', array('placeholder' => 'Correo')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textField($model,'city', array('placeholder' => 'Ciudad o País')); ?>
                </div>
                <div class="row">
                    <?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50, 'placeholder' => 'Comentario')); ?>
                </div>

                <?php if(CCaptcha::checkRequirements()): ?>
                    <div class="row">
                        <div>
                            <?php $this->widget('CCaptcha'); ?>
                            <?php
                                echo $form->textField($model,'verifyCode',
                                    array(
                                        'placeholder'   => 'Código',
                                        'class'         => 'small-7 large-7 columns float-right',
                                    )
                                );
                            ?>
                            <?php echo $form->error($model,'verifyCode'); ?>
                         </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <?php
                        echo CHtml::submitButton('Contact',
                            array(
                                'class' => 'button expanded',
                                'id'    => 'btn_contact',
                                'value' => 'Contactar'
                            )
                        );
                    ?>
                </div>
            <?php $this->endWidget(); ?>
        </div>

        <div class="small-12 medium-4 large-4 columns">
            <ul class="menu vertical">
               <!--  <li><a href="<?php //echo Yii::app()->createUrl('prevencion-infecciones-'.Constants::Cat_prevencion_infecc); ?>"><span>Prevención de Infecciones</span></a></li>
                <li><a href="<?php //echo Yii::app()->createUrl('cuidado-avanzado-de-heridas-'.Constants::Cat_cuidado_heridas); ?>"><span>Cuidado Avanzado de Heridas</span></a></li>
                <li><a href="<?php //echo Yii::app()->createUrl('material-de-curacion-'.Constants::Cat_materia_curacion); ?>"><span>Material de Curación</span></a></li>
                <li><a href="<?php //echo Yii::app()->createUrl('ostomias-'.Constants::Cat_ostomias); ?>"><span>Ostomías</span></a></li>
                <li><a href="<?php //echo Yii::app()->createUrl('hipodermicos-'.Constants::Cat_hipodermicos); ?>"><span>Hipodérmicos</span></a></li>
                <li><a href="<?php //echo Yii::app()->createUrl('quirurgico-'.Constants::Cat_quirurgico); ?>"><span>Quirúrgicos</span></a></li>
                <li><a href="<?php //echo Yii::app()->createUrl('otros-'.Constants::Cat_otros); ?>"><span>Otros</span></a></li> -->
<!--                 <li><a href="terminos-condiciones.php"><span>Términos y condiciones de compra</span></a></li>
 --><!--                 <li><a href="politicas-envio.php"><span>Políticas de envío</span></a></li>
 -->                <li><a href="aviso-privacidad.php"><span>Aviso de privacidad</span></a></li>
            </ul>
        </div>
        <div class="small-12 medium-4 large-4 columns">
            <h4 class="text-center">Más información</h4>
            <p>Matriz Ciudad de México</p>
            <p>Pronamac S.A. de C.V. <br>
                5 de Febrero N° 809, Col. Álamos, Delegación Benito Juárez Mexico, Distrito Federal 03400
            </p>
            <p>Tel: 5698 - 0430</p>
            <p>Email: ventas_pronamac@prodigy.net.mx</p>
        </div>
    </div>
</footer>
