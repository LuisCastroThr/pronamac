<div class="reveal modal-page" id="modalLogin" data-reveal>
    <p class="title-modal">Inicio de Sesión</p>
    <p class="text-center">¿Nuevo usuario? <a data-open="modalRegister">Crear cuenta</a></p>

    <?php 
        $model = new LoginForm;
        $form = $this->beginWidget('CActiveForm', array(
            'id'                    => 'login-form',
            'enableClientValidation'=> true,
            'clientOptions'         => array(
                'validateOnSubmit'  => true,
            )
        )); 
    ?>
        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-envelope" aria-hidden="true"></i></span>
            <?php 
                echo $form->textField($model,'username', array(
                        'class'         => 'input-group-field',
                        'placeholder'   => 'Correo'
                    )
                ); 
            ?>
        </div>

        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
            <?php 
                echo $form->passwordField($model,'password', array(
                        'class'         => 'input-group-field',
                        'placeholder'   => 'Contraseña'
                    )
                ); 
            ?>
        </div>

        <div class="medium-12 columns">
            <?php 
                echo CHtml::submitButton('Login', array(
                        'class' => 'button expanded',
                        'value' => 'Iniciar sesión',
                        'id'    => 'btn_login'
                    )
                ); 
            ?>
        </div>
        <p class="text-center"><a>¿Olvidaste tu contraseña?</a></p>
    <?php $this->endWidget(); ?>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>


<div class="reveal modal-page" id="modalRegister" data-reveal>
    <p class="title-modal">Registro</p>

    <?php
        $model = new RegisterForm; 
        $form  = $this->beginWidget('CActiveForm', 
        array(
            'id' => 'pnc-users-form'
        )); 
    ?>
        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-user" aria-hidden="true"></i></span>
            <?php 
                echo $form->textField($model, 'name',
                    array(
                        'size'        => 60,
                        'maxlength'   => 100,
                        'class'       => 'input-group-field',
                        'placeholder' => 'Nombre'
                    )
                ); 
            ?>
        </div>

        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-envelope" aria-hidden="true"></i></span>
            <?php 
                echo $form->textField($model, 'email',
                    array(
                        'size'        => 60,
                        'maxlength'   => 100,
                        'class'       => 'input-group-field',
                        'placeholder' => 'Correo'
                    )
                ); 
            ?>
        </div>

        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
            <?php 
                echo $form->passwordField($model, 'password',
                    array(
                        'size'        => 60,
                        'maxlength'   => 200,
                        'class'       => 'input-group-field',
                        'placeholder' => 'Contraseña'
                    )
                ); 
            ?>
        </div>

        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
            <?php 
                echo $form->passwordField($model, 'repassword',
                    array(
                        'size'        => 60,
                        'maxlength'   => 200,
                        'class'       => 'input-group-field',
                        'placeholder' => 'Confirmar ontraseña'
                    )
                ); 
            ?>
        </div>

        <div class="medium-12 columns">
            <input type="checkBox" id="RegisterForm_terminosCondiciones" name="RegisterForm[terminosCondiciones]">
            <label for="PncUsers_terminosCondiciones">Acepto <a>términos y condiciones</a></label>
        </div>
        <div class="medium-12 columns">
            <?php 
                echo CHtml::submitButton('Save',array(
                        'value' => 'Registrarse',
                        'class' => 'button expanded',
                        'id'    => 'btn_register'
                    )
                ); 
            ?>
        </div>
    <?php $this->endWidget(); ?>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>


<div class="reveal modal-page" id="modalRecuperar" data-reveal>
    <p class="title-modal">Recuperar contraseña</p>
    <p class="text-center">Ingresa correo electrónico</p>

    <form>
        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-envelope" aria-hidden="true"></i></span>
            <input class="input-group-field is-invalid-input" type="text" placeholder="Ingresa correo">
        </div>

        <div class="medium-12 columns">
            <input class="button expanded" type="submit" name="" value="Recuperar contraseña">
        </div>
    </form>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>


<div class="reveal modal-page" id="modalChangePassword" data-reveal>
    <p class="title-modal">Recuperar contraseña</p>
    <p class="text-center">Cambia tu contraseña</p>

    <form>
        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
            <input class="input-group-field" type="password" placeholder="Contraseña">
        </div>

        <div class="input-group medium-12 columns">
            <span class="input-group-label"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
            <input class="input-group-field" type="password" placeholder="Contraseña">
        </div>

        <div class="medium-12 columns">
            <input class="button expanded" type="submit" name="" value="Recuperar contraseña">
        </div>
    </form>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>