<div class="wrapper-general row">
	<h1 class="small-12 large-12 columns h1-text">Datos de Compra</h1>
	<div class="small-9 large-9 columns">
		<div class="section-step">
			<p class="title-step">
				<span class="badge">1</span><span class="h2-text">Opciones de Envío</span>
			</p>

			<div class="select-option row">
					<div class="medium-4 columns">
						<input name="type_send" type="radio" id="send-2">
						<label for="send-2" class="option-send select-send">
							Envíar a dirección
						</label>
					</div>
					<div class="medium-4 columns">
						<input name="type_send" type="radio" id="send-1">
						<label for="send-1" class="option-send select-pick-up">
							Recoger en sucursal
						</label>
					</div>
					<div class="medium-4 columns">
					</div>
				</div>

			<?php if(!empty($location)): ?>
				<div class="subsection-step address-send" style="display:none;">
					<p class="h2-text">Dirección de envío <small>Tiempo de entrega: 1 a 3 días hábiles</small></p>
					<div class="select-addres active">
						<input class="float-right" type="radio" name="sucursal" value="<?php echo Constants::opt_owner_user; ?>" id="select-addres">
						<label for="select-addres">
							<!-- <p>Dirección 1</p> -->
							<p>
								<?php echo $location->calle; ?> <?php echo $location->ext_numero; ?>, 
								Col. <?php echo $location->colonia; ?>, 
								C.P. <?php echo $location->codigo_postal; ?>, 
								Del. <?php echo $location->delegacion; ?>, 
								<?php echo Constants::$states[$location->estado]; ?>.
							</p>
						</label>
					</div>
					<button class="button pull-right new-address success" type="button" name="button">Actualizar Dirección</button>
				</div>
			<?php else: ?>
				<div class="subsection-step address-send" style="display:;">					
					<button class="button pull-right new-address success" type="button" name="button">Agregar Nueva Dirección</button>
				</div>
			<?php endif; ?>

			<?php $this->renderPartial('/perfil/location', array('location'=>$location)); ?>

			<div class="subsection-step select-local" style="display: none;">
				<p class="h2-text">Selecciona Sucursal</p>
				<div class="select-addres">
					<input class="float-right" type="radio" name="sucursal" value="<?php echo Constants::opt_sucur_cdmx; ?>" id="select-mexico">
					<label for="select-mexico">
						<p class="subtitle"><strong>Matriz Ciudad de México</strong> | Pronamac, S.A. de C.V.</p>
						<p class="truncate">5 de Febrero N° 809, Col. Álamos, Delegación Benito Juárez, C.P. 03400 Ciudad de México</p>
					</label>
				</div>

				<div class="select-addres">
					<input class="float-right" type="radio" name="sucursal" value="<?php echo Constants::opt_sucur_jalisco; ?>" id="select-jalisco">
					<label for="select-jalisco">
						<p class="subtitle"><strong>Sucursal Zapopan, Jalisco</strong> | Pronamac de Occidente</p>
						<p class="truncate">Giosue Carducci No.5541 Col. Jardines Vallarta, C.P. 45150 Zapopan, Jalisco</p>
					</label>
				</div>

				<div class="select-addres">
					<input class="float-right" type="radio" name="sucursal" value="<?php echo Constants::opt_sucur_monterrey; ?>" id="select-mty">
					<label for="select-mty">
						<p class="subtitle"><strong>Sucursal Monterrey </strong></p>
						<p class="truncate">Gregorio Salinas Varona No. 250 Col. Burócratas del Estado C.P. 64380 Monterrey  N.L</p>
					</label>
				</div>
			</div>
		</div>

		<div class="section-step">
			<p class="title-step">
				<span class="badge">2</span><span class="h2-text">Solicitar Facturar</span>
			</p>

			<div class="select-option row">
				<div class="medium-4 columns">
					<input type="radio" name="facturas" id="facturar" value="1">
					<label for="facturar" class="option-send check-in">
						Solicitar factura
					</label>
				</div>
				<div class="medium-4 columns">
					<input type="radio" name="facturas" id="no-facturar" value="2">
					<label for="no-facturar" class="option-send no-bill">
						No solicitar factura
					</label>
				</div>
				<div class="medium-4 columns">
				</div>
			</div>

			<div class="subsection-step show-check-in" style="display: none;">
				<?php if(!empty($factura)): ?>
					<p class="h2-text">Datos de Facturación</p>
					<div class="select-addres active">
						<input type="checkbox" name="invoices" value="" id="factuacion">
						<label for="factuacion">
							<div class="medium-6 columns">
								<p><strong>Nombre ó Razón social:</strong> <?php echo $factura->razon; ?></p>
								<p><strong>RFC:</strong> <?php echo $factura->rfc; ?></p>
								<p><strong>Dirección Fiscal:</strong> 
									<?php echo $factura->calle; ?> <?php echo $factura->ext_numero; ?>, 
									Col. <?php echo $factura->colonia; ?>, 
									C.P. <?php echo $factura->codigo_postal; ?>, 
									Del. <?php echo $factura->delegacion; ?>, 
									<?php echo Constants::$states[$factura->estado]; ?></p>.
							</div>
							<div class="medium-6 columns">
								<p><strong>Tipo de Facturación:</strong> <?php echo Constants::$factura_tipo[$factura->tipo]; ?></p>
								<p><strong>Teléfono:</strong> <?php echo $factura->telefono; ?></p>
								<p><strong>Correo:</strong> <?php echo $factura->correo; ?></p>
							</div>
						</label>
					</div>
				<?php endif; ?>
				<button class="button pull-right new-check-in" type="button" name="button">Agregar Nuevos Datos</button>
			</div>

			<div class="subsection-step generate-check-in" style="display: none;">
				<p class="h2-text">Ingresa datos de facturación</p>
				<?php $this->renderPartial('/perfil/factura', array('factura'=>$factura)); ?>
			</div>
		</div>


	</div>

	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/aside-cart', array('detalle'=>$detalle)); ?>
	</div>
</div>
