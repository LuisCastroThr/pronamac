<div class="wrapper-general row">
	<h1 class="small-12 large-12 columns h1-text">Datos para depósito en cuenta:</h1>
	<div class="small-9 large-9 columns">

		<span class="h2-text"><strong>Monto a depositar: $ <?php echo number_format($carrito->total, 2, '.',','); ?></strong></span>

		<div class="table row">
			<ul class="row" style="list-style: none; clear: both; margin: 30px 0 0;">
				<li class="small-4 large-4 columns">
					# REFERENCIA:
				</li>
				<li class="small-8 large-8 columns">
					<?php echo $referencia; ?>
				</li>
			</ul>

			<ul class="row" style="list-style: none; clear: both;">
				<li class="small-4 large-4 columns">
					EMPRESA:
				</li>
				<li class="small-8 large-8 columns">
					Proveedora Nacional de Material de Curación, S.A. de C.V.
				</li>
			</ul>

			<ul class="row" style="list-style: none; clear: both;">
				<li class="small-4 large-4 columns">
					RFC:
				</li>
				<li class="small-8 large-8 columns">
					PNM8604219NA
				</li>
			</ul>

			<ul class="row" style="list-style: none; clear: both;">
				<li class="small-4 large-4 columns">
					DIRECCION:
				</li>
				<li class="small-8 large-8 columns">
					5 de Febrero No. 809, Col. Álamos, Del. Benito Juárez, Ciudad de México, C.P. 03400
				</li>
			</ul>

			<ul class="row" style="list-style: none; clear: both;">
				<li class="small-4 large-4 columns">
					BANCO:
				</li>
				<li class="small-8 large-8 columns">
					Banamex
				</li>
			</ul>

			<ul class="row" style="list-style: none; clear: both;">
				<li class="small-4 large-4 columns">
					CUENTA:
				</li>
				<li class="small-8 large-8 columns">
					6942591
				</li>
			</ul>

			<ul class="row" style="list-style: none; clear: both;">
				<li class="small-4 large-4 columns">
					CLABE:
				</li>
				<li class="small-8 large-8 columns">
					002180023669425918
				</li>
			</ul>
		</div>

		<br>

		<p>
			Cuenta con un plazo máximo de 24 hrs. para realizar el pago de pedido o su compra será cancelada.
			Compruebe su depósito al correo electrónico <a href="mailto:cotizaciones4@pronamac.com">cotizaciones4@pronamac.com</a>  
		</p>
	</div>
	
	<div class="small-3 large-3 columns">
	</div>
</div>
