<div class="wrapper-general row">
	<h1 class="small-12 large-12 columns h1-text">Carrito de Compras</h1>
	<div class="small-9 large-9 columns update_item_cart">
		<div class="section-step">
			<?php foreach($itemCart as $item): ?>
				<div class="list-products div_<?php echo $item['id']; ?>">
					<div class="small-2 large-2 columns">
						<div class="img-product">
							<?php if(isset($item['image']) && !empty($item['image'])): ?>
								<img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/product/<?php echo $item['image']; ?>" alt="">
							<?php else: ?>
								<img src="<?php echo Yii::app()->theme->baseUrl; ?>/src/product/default.jpg" alt="">
							<?php endif; ?>
						</div>
					</div>

					<div class="description small-4 large-4 columns">
						<p><strong><?php echo $item['producto']; ?></strong></p>
						<p>Piezas: <?php echo $item['presenta']; ?></p>
						<p>Precio Unitario: $ <?php echo number_format($item['precio'],2); ?></p>
					</div>

					<div class="small-2 large-2 columns text-center">
						<p class="price">$ <?php echo number_format($item['total'],2); ?></p>
					</div>

					<div class="small-3 large-3 columns">
						<div class="select-product">
							<button type="button" name="button" class="small-3 columns select-number">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</button>
							<input class="small-6 columns" type="text" name="" value="<?php echo $item['cantidad']; ?>" placeholder="0" data-id="<?php echo $item['id']; ?>">
							<button type="button" name="button" class="small-3 columns select-number">
								<i class="fa fa-plus" aria-hidden="true"></i>
							</button>
						</div>
					</div>

					<div class="small-1 large-1 columns ">
						<button class="erase-article" data-id="<?php echo $item['id']; ?>" type="button" name="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

	<div class="small-3 large-3 columns">
		<?php $this->renderPartial('/site/templates/aside-cart', array('detalle' => $detalle)); ?>
	</div>
</div>
